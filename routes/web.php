<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::get('/login', 'App\Http\Controllers\LoginController@index')->name('login');
Route::get('/logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::post('/process-login', 'App\Http\Controllers\LoginController@process_login')->name('process-login');

Route::group(['middleware' => 'admin'], function () {
	Route::group(['middleware' => 'admin_balai'], function () {
		Route::get('/jenis-kegiatan', 'App\Http\Controllers\JenisKegiatanController@index')->name('jenis-kegiatan');
		Route::get('/add-jenis-kegiatan', 'App\Http\Controllers\JenisKegiatanController@add_jenis_kegiatan')->name('add-jenis-kegiatan');
		Route::get('/edit-jenis-kegiatan/{id}', 'App\Http\Controllers\JenisKegiatanController@add_jenis_kegiatan')->name('edit-jenis-kegiatan');
		Route::get('/delete-jenis-kegiatan/{id}', 'App\Http\Controllers\JenisKegiatanController@delete_jenis_kegiatan')->name('delete-jenis-kegiatan');
		Route::post('/create-jenis-kegiatan', 'App\Http\Controllers\JenisKegiatanController@created_jenis_kegiatan')->name('create-jenis-kegiatan');

		Route::get('/kategori', 'App\Http\Controllers\KategoriController@index')->name('kategori');
		Route::get('/add-kategori', 'App\Http\Controllers\KategoriController@add_kategori')->name('add-kategori');
		Route::get('/edit-kategori/{id}', 'App\Http\Controllers\KategoriController@add_kategori')->name('edit-kategori');
		Route::get('/delete-kategori/{id}', 'App\Http\Controllers\KategoriController@delete_kategori')->name('delete-kategori');
		Route::post('/create-kategori', 'App\Http\Controllers\KategoriController@created_kategori')->name('create-kategori');

		Route::get('/sektor', 'App\Http\Controllers\SektorController@index')->name('sektor');
		Route::get('/add-sektor', 'App\Http\Controllers\SektorController@add_sektor')->name('add-sektor');
		Route::get('/add-setting-document/{id}', 'App\Http\Controllers\SektorController@add_setting_document')->name('add-setting-document');
		Route::get('/edit-sektor/{id}', 'App\Http\Controllers\SektorController@add_sektor')->name('edit-sektor');
		Route::get('/delete-sektor/{id}', 'App\Http\Controllers\SektorController@delete_sektor')->name('delete-sektor');
		Route::post('/create-sektor', 'App\Http\Controllers\SektorController@created_sektor')->name('create-sektor');
		Route::post('/create-setting-document', 'App\Http\Controllers\SektorController@created_setting_document')->name('create-setting-document');

		Route::get('/kabupaten', 'App\Http\Controllers\WilayahController@kabupaten')->name('kabupaten');
		Route::get('/add-kabupaten', 'App\Http\Controllers\WilayahController@add_kabupaten')->name('add-kabupaten');
		Route::get('/edit-kabupaten/{id}', 'App\Http\Controllers\WilayahController@add_kabupaten')->name('edit-kabupaten');
		Route::get('/delete-kabupaten/{id}', 'App\Http\Controllers\WilayahController@delete_kabupaten')->name('delete-kabupaten');
		Route::post('/create-kabupaten', 'App\Http\Controllers\WilayahController@created_kabupaten')->name('create-kabupaten');

		Route::get('/provinsi', 'App\Http\Controllers\WilayahController@provinsi')->name('provinsi');
		Route::get('/add-provinsi', 'App\Http\Controllers\WilayahController@add_provinsi')->name('add-provinsi');
		Route::get('/edit-provinsi/{id}', 'App\Http\Controllers\WilayahController@add_provinsi')->name('edit-provinsi');
		Route::get('/delete-provinsi/{id}', 'App\Http\Controllers\WilayahController@delete_provinsi')->name('delete-provinsi');
		Route::post('/create-provinsi', 'App\Http\Controllers\WilayahController@created_provinsi')->name('create-provinsi');

		Route::get('/tahapan-kegiatan', 'App\Http\Controllers\TahapanKegiatanController@index')->name('tahapan-kegiatan');
		Route::get('/add-tahapan-kegiatan', 'App\Http\Controllers\TahapanKegiatanController@add_tahapan_kegiatan')->name('add-tahapan-kegiatan');
		Route::get('/edit-tahapan-kegiatan/{id}', 'App\Http\Controllers\TahapanKegiatanController@add_tahapan_kegiatan')->name('edit-tahapan-kegiatan');
		Route::get('/delete-tahapan-kegiatan/{id}', 'App\Http\Controllers\TahapanKegiatanController@delete_tahapan_kegiatan')->name('delete-tahapan-kegiatan');
		Route::post('/create-tahapan-kegiatan', 'App\Http\Controllers\TahapanKegiatanController@created_tahapan_kegiatan')->name('create-tahapan-kegiatan');

		Route::get('/add-profiles', 'App\Http\Controllers\ProfilesController@add_profiles')->name('add-profiles');
		Route::get('/edit-profiles/{id}', 'App\Http\Controllers\ProfilesController@add_profiles')->name('edit-profiles');
		Route::get('/delete-profiles/{id}', 'App\Http\Controllers\ProfilesController@delete_profiles')->name('delete-profiles');
		Route::post('/create-profiles', 'App\Http\Controllers\ProfilesController@created_profiles')->name('create-profiles');
		Route::post('/create-slide', 'App\Http\Controllers\ProfilesController@create_slide')->name('create-slide');
		Route::get('/delete-slide/{id}', 'App\Http\Controllers\ProfilesController@delete_slide')->name('delete-slide');

		Route::get('/paket', 'App\Http\Controllers\PaketController@index')->name('paket');
		Route::get('/add-paket', 'App\Http\Controllers\PaketController@add_paket')->name('add-paket');
		Route::get('/edit-paket/{id}', 'App\Http\Controllers\PaketController@add_paket')->name('edit-paket');
		Route::get('/delete-paket/{id}', 'App\Http\Controllers\PaketController@delete_paket')->name('delete-paket');
		Route::post('/create-paket', 'App\Http\Controllers\PaketController@created_paket')->name('create-paket');
		
		Route::get('/users', 'App\Http\Controllers\UsersKotaKabController@index')->name('users');
		Route::get('/add-users', 'App\Http\Controllers\UsersKotaKabController@add_users')->name('add-users');
		Route::get('/edit-users/{id}', 'App\Http\Controllers\UsersKotaKabController@add_users')->name('edit-users');
		Route::get('/delete-users/{id}', 'App\Http\Controllers\UsersKotaKabController@delete_users')->name('delete-users');
		Route::post('/create-users', 'App\Http\Controllers\UsersKotaKabController@created_users')->name('create-users');

		Route::get('/delete-perencanaan/{id}', 'App\Http\Controllers\PerencanaanController@delete_perencanaan')->name('delete-perencanaan');
		
		Route::get('/nilai-perencanaan/{id}', 'App\Http\Controllers\PerencanaanController@nilai_perencanaan')->name('nilai-perencanaan');
		Route::post('/update-penilaian-perencanaan', 'App\Http\Controllers\PerencanaanController@update_penilaian_perencanaan')->name('update-penilaian-perencanaan');
	
		Route::get('/export-pdf-usulan-paket', 'App\Http\Controllers\LaporanController@export_pdf_usulan_paket')->name('export-pdf-usulan-paket');
		Route::get('/export-excel-usulan-paket', 'App\Http\Controllers\LaporanController@export_excel_usulan_paket')->name('export-excel-usulan-paket');
	});

	Route::group(['middleware' => 'admin_kota'], function () {
		Route::get('/add-perencanaan', 'App\Http\Controllers\PerencanaanController@add_perencanaan')->name('add-perencanaan');
		Route::get('/edit-perencanaan/{id}', 'App\Http\Controllers\PerencanaanController@add_perencanaan')->name('edit-perencanaan');
		
		Route::get('/delete-file/{id}/{id_perencanaan}', 'App\Http\Controllers\PerencanaanController@delete_file')->name('delete-file');
		Route::post('/create-perencanaan', 'App\Http\Controllers\PerencanaanController@created_perencanaan')->name('create-perencanaan');
		Route::post('/save-upload-file', 'App\Http\Controllers\PerencanaanController@save_upload_file')->name('save-upload-file');
		Route::post('/get-file-document-per-paket', 'App\Http\Controllers\PaketController@get_file_document_per_paket')->name('get-file-document-per-paket');
	});

	Route::get('/', 'App\Http\Controllers\DashboardController@index');
	Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
	Route::get('/profile', 'App\Http\Controllers\ProfileController@index')->name('profile');
	Route::post('/update-profile', 'App\Http\Controllers\ProfileController@update_profile')->name('update-profile');
	Route::get('/change-password', 'App\Http\Controllers\ProfileController@change_password')->name('change-password');
	Route::post('/update-password', 'App\Http\Controllers\ProfileController@update_password')->name('update-password');

	Route::get('/profiles', 'App\Http\Controllers\ProfilesController@index')->name('profiles');
	Route::get('/perencanaan', 'App\Http\Controllers\PerencanaanController@index')->name('perencanaan');
	Route::get('/lap-daftar-usulan-paket', 'App\Http\Controllers\LaporanController@daftar_usulan_paket')->name('lap-daftar-usulan-paket');
});
