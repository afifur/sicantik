<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class LapDaftarUsulanPaketExport implements FromView
{
    protected $data;

	function __construct($data) {
	    $this->data = $data;
	}

    public function view(): View
    {
        return view('laporan.excel-daftar-usulan-paket', [
            'data' => $this->data,
        ]);
    }
}
