<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kabupaten;
use App\Models\Provinsi;

class WilayahController extends Controller
{
    public function kabupaten()
    {
    	$data = Kabupaten::join('provinsi', 'kabupaten.provinsi_id', '=', 'provinsi.id')->get();
    	return view('masterdata.kabupaten-index', compact('data'));
    }

    public function add_kabupaten($id=NULL)
    {
    	$kabupaten = Kabupaten::where('id', $id)->first();
        $provinsi = Provinsi::where('status', 1)->get();
      	
        return view('masterdata.add-kabupaten', compact('kabupaten', 'provinsi'));
    }

    public function created_kabupaten(Request $request)
    {
    	$validated = $request->validate([
	        'nama_kabupaten' => 'required'
	    ]);

        $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('kabupaten')->insert([
                    'provinsi_id' => $request->input('provinsi_id'),
                    'nama_kabupaten' => $request->input('nama_kabupaten'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('kabupaten')->where('id', $id)->update([
                    'provinsi_id' => $request->input('provinsi_id'),
                    'nama_kabupaten' => $request->input('nama_kabupaten'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('kabupaten'))->with('success', 'sukses, data kabupaten berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-kabupaten'))->with('error', 'data kabupaten gagal disimpan');
	    }
    }

    public function delete_kabupaten($id)
    {
    	$post = Kabupaten::find($id);
   		if($post->delete()){
   			return redirect(route('kabupaten'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('kabupaten'))->with('error', 'data gagal dihapus');
   		}
    }

    public function provinsi()
    {
    	$data = Provinsi::get();
    	return view('masterdata.provinsi-index', compact('data'));
    }

    public function add_provinsi($id=NULL)
    {
    	$provinsi = Provinsi::where('id', $id)->first();
        $data = Provinsi::get();
      	
        return view('masterdata.add-provinsi', compact('provinsi', 'data'));
    }

    public function created_provinsi(Request $request)
    {
    	$validated = $request->validate([
	        'nama_provinsi' => 'required'
	    ]);

        $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('provinsi')->insert([
                    'nama_provinsi' => $request->input('nama_provinsi'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('provinsi')->where('id', $id)->update([
                    'nama_provinsi' => $request->input('nama_provinsi'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('add-provinsi'))->with('success', 'sukses, data provinsi berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-provinsi'))->with('error', 'data provinsi gagal disimpan');
	    }
    }

    public function delete_provinsi($id)
    {
    	$post = Provinsi::find($id);
   		if($post->delete()){
   			return redirect(route('add-provinsi'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('add-provinsi'))->with('error', 'data gagal dihapus');
   		}
    }
}
