<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function index()
    {
    	$data = Kategori::get();
    	return view('masterdata.kategori-index', compact('data'));
    }

    public function add_kategori($id=NULL)
    {
    	$kategori = Kategori::where('id', $id)->first();
      	
        return view('masterdata.add-kategori', compact('kategori'));
    }

    public function created_kategori(Request $request)
    {
    	$validated = $request->validate([
	        'nama_kategori' => 'required'
	    ]);

        $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('kategori')->insert([
                    'kode_kategori' => $request->input('kode_kategori'),
                    'nama_kategori' => $request->input('nama_kategori'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('kategori')->where('id', $id)->update([
                    'kode_kategori' => $request->input('kode_kategori'),
                    'nama_kategori' => $request->input('nama_kategori'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('kategori'))->with('success', 'sukses, data kategori berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-kategori'))->with('error', 'data kategori gagal disimpan');
	    }
    }

    public function delete_kategori($id)
    {
    	$post = Kategori::find($id);
   		if($post->delete()){
   			return redirect(route('kategori'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('kategori'))->with('error', 'data gagal dihapus');
   		}
    }
}
