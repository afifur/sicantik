<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Profile;
use App\Models\Content;

class ProfilesController extends Controller
{
    public function index()
    {
    	$profiles = Profile::where('id', 1)->first();
        $contents = Content::get();

    	return view('perencanaan.profiles-index', compact('profiles', 'contents'));
    }

    public function add_profiles($id=NULL)
    {
    	$profiles = Profile::where('id', $id)->first();
      	
        return view('perencanaan.add-profiles', compact('profiles'));
    }

    public function created_profiles(Request $request)
    {
        $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		$insertID = DB::table('profile')->insertGetId([
                    'title' => $request->input('title'),
                    'deskripsi' => $request->input('deskripsi'),
                    'nama_kepala_balai1' => $request->input('nama_kepala_balai1'),
                    'jabatan_kepala_balai1' => $request->input('jabatan_kepala_balai1'),
                    'nama_kepala_balai2' => $request->input('nama_kepala_balai2'),
                    'jabatan_kepala_balai2' => $request->input('jabatan_kepala_balai2'),
                    'nama_kasubag' => $request->input('nama_kasubag'),
                    'jabatan_kasubag' => $request->input('jabatan_kasubag'),
                    'nama_kasie_wil1' => $request->input('nama_kasie_wil1'),
                    'jabatan_kasie_wil1' => $request->input('jabatan_kasie_wil1'),
                    'nama_kasie_wil2' => $request->input('nama_kasie_wil2'),
                    'jabatan_kasie_wil2' => $request->input('jabatan_kasie_wil2'),
                    'nama_kasatker' => $request->input('nama_kasatker'),
                    'jabatan_kasatker' => $request->input('jabatan_kasatker'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

				if ($request->hasFile('foto_kepala_balai1') == true) {
                    $this->upload_foto_kepala_balai1($request, $insertID);
                }

                if ($request->hasFile('foto_kasubag') == true) {
                    $this->upload_foto_kasubag($request, $insertID);
                }

                if ($request->hasFile('foto_kasie_wil1') == true) {
                    $this->upload_foto_kasie_wil1($request, $insertID);
                }

                if ($request->hasFile('foto_kasie_wil2') == true) {
                    $this->upload_foto_kasie_wil2($request, $insertID);
                }

                if ($request->hasFile('foto_kasatker') == true) {
                    $this->upload_foto_kasatker($request, $insertID);
                }
	    	}else{
	    		DB::table('profile')->where('id', $id)->update([
                    'title' => $request->input('title'),
                    'deskripsi' => $request->input('deskripsi'),
                    'nama_kepala_balai1' => $request->input('nama_kepala_balai1'),
                    'jabatan_kepala_balai1' => $request->input('jabatan_kepala_balai1'),
                    'nama_kepala_balai2' => $request->input('nama_kepala_balai2'),
                    'jabatan_kepala_balai2' => $request->input('jabatan_kepala_balai2'),
                    'nama_kasubag' => $request->input('nama_kasubag'),
                    'jabatan_kasubag' => $request->input('jabatan_kasubag'),
                    'nama_kasie_wil1' => $request->input('nama_kasie_wil1'),
                    'jabatan_kasie_wil1' => $request->input('jabatan_kasie_wil1'),
                    'nama_kasie_wil2' => $request->input('nama_kasie_wil2'),
                    'jabatan_kasie_wil2' => $request->input('jabatan_kasie_wil2'),
                    'nama_kasatker' => $request->input('nama_kasatker'),
                    'jabatan_kasatker' => $request->input('jabatan_kasatker'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);

				if ($request->hasFile('foto_kepala_balai1') == true) {
                    $this->upload_foto_kepala_balai1($request, $id);
                }


                if ($request->hasFile('foto_kasubag') == true) {
                    $this->upload_foto_kasubag($request, $id);
                }

                if ($request->hasFile('foto_kasie_wil1') == true) {
                    $this->upload_foto_kasie_wil1($request, $id);
                }

                if ($request->hasFile('foto_kasie_wil2') == true) {
                    $this->upload_foto_kasie_wil2($request, $id);
                }

                if ($request->hasFile('foto_kasatker') == true) {
                    $this->upload_foto_kasatker($request, $id);
                }
	    	}

	    	DB::commit();
            return redirect(route('profiles'))->with('success', 'sukses, data profile berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-profiles'))->with('error', 'data profile gagal disimpan');
	    }
    }

    public function create_slide(Request $request)
    {
        $id = $request->input('id');

        DB::beginTransaction();

        try {
            
            if ($id == NULL OR $id == '') {
                $insertID = DB::table('content')->insertGetId([
                    'type_content' => 1,
                    'title_content' => '-',
                    'desc_content' => '-',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

                if ($request->hasFile('file_content') == true) {
                    $this->upload_file_slide($request, $insertID);
                }

            }else{
                DB::table('content')->where('id', $id)->update([
                    'type_content' => 1,
                    'title_content' => '-',
                    'desc_content' => '-',
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);

                if ($request->hasFile('file_content') == true) {
                    $this->upload_file_slide($request, $id);
                }
            }

            DB::commit();
            return redirect(route('profiles'))->with('success', 'sukses, data berhasil disimpan');
        } catch (Exception $e) {
            DB::rollback();
            return redirect(route('add-profiles'))->with('error', 'data gagal disimpan');
        }
    }

    public function delete_slide($id)
    {
        $post = Content::where('id', $id)->where('type_content', 1);
        if($post->delete()){
            return redirect(route('profiles'))->with('success', 'sukses, data berhasil dihapus');
        }else{
            return redirect(route('profiles'))->with('error', 'data gagal dihapus');
        }
    }

    public function upload_file_slide($request, $id)
    {
        $request->validate([
            'file_content' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/slide/');
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('file_content')) {
            $imageName = time().'.'.$request->file_content->extension();  
            $createdImage = $request->file_content->move($path, $imageName);

            if ($createdImage) {
                $created_data = Content::where('id', $id)->update([
                    'file_content' => $imageName,
                ]);
            }
        }
    }

    public function upload_foto_kepala_balai1($request, $id)
    {
    	$request->validate([
            'foto_kepala_balai1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/foto/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('foto_kepala_balai1')) {
            $imageName = time().'.'.$request->foto_kepala_balai1->extension();  
            $createdImage = $request->foto_kepala_balai1->move($path, $imageName);

            if ($createdImage) {
                $created_data = Profile::where('id', $id)->update([
                    'foto_kepala_balai1' => $imageName,
                ]);
            }
        }
    }

    public function upload_foto_kasubag($request, $id)
    {
    	$request->validate([
            'foto_kasubag' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/foto/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('foto_kasubag')) {
            $imageName = time().'.'.$request->foto_kasubag->extension();  
            $createdImage = $request->foto_kasubag->move($path, $imageName);

            if ($createdImage) {
                $created_data = Profile::where('id', $id)->update([
                    'foto_kasubag' => $imageName,
                ]);
            }
        }
    }

    public function upload_foto_kasie_wil1($request, $id)
    {
    	$request->validate([
            'foto_kasie_wil1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/foto/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('foto_kasie_wil1')) {
            $imageName = time().'.'.$request->foto_kasie_wil1->extension();  
            $createdImage = $request->foto_kasie_wil1->move($path, $imageName);

            if ($createdImage) {
                $created_data = Profile::where('id', $id)->update([
                    'foto_kasie_wil1' => $imageName,
                ]);
            }
        }
    }

    public function upload_foto_kasie_wil2($request, $id)
    {
    	$request->validate([
            'foto_kasie_wil2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/foto/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('foto_kasie_wil2')) {
            $imageName = time().'.'.$request->foto_kasie_wil2->extension();  
            $createdImage = $request->foto_kasie_wil2->move($path, $imageName);

            if ($createdImage) {
                $created_data = Profile::where('id', $id)->update([
                    'foto_kasie_wil2' => $imageName,
                ]);
            }
        }
    }

    public function upload_foto_kasatker($request, $id)
    {
    	$request->validate([
            'foto_kasatker' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);

        $path = public_path('uploads/foto/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->hasFile('foto_kasatker')) {
            $imageName = time().'.'.$request->foto_kasatker->extension();  
            $createdImage = $request->foto_kasatker->move($path, $imageName);

            if ($createdImage) {
                $created_data = Profile::where('id', $id)->update([
                    'foto_kasatker' => $imageName,
                ]);
            }
        }
    }
}
