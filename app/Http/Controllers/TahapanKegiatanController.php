<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\TahapanKegiatan;

class TahapanKegiatanController extends Controller
{
    public function index()
    {
    	$data = TahapanKegiatan::get();
    	return view('masterdata.tahapan-kegiatan-index', compact('data'));
    }

    public function add_tahapan_kegiatan($id=NULL)
    {
    	$kegiatan = TahapanKegiatan::where('id', $id)->first();
      	
        return view('masterdata.add-tahapan-kegiatan', compact('kegiatan'));
    }

    public function created_tahapan_kegiatan(Request $request)
    {
    	$validated = $request->validate([
	        'nama_tahapan_kegiatan' => 'required'
	    ]);

	    $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('tahapan_kegiatan')->insert([
                    'kode_tahapan_kegiatan' => $request->input('kode_tahapan_kegiatan'),
                    'nama_tahapan_kegiatan' => $request->input('nama_tahapan_kegiatan'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('tahapan_kegiatan')->where('id', $id)->update([
                    'kode_tahapan_kegiatan' => $request->input('kode_tahapan_kegiatan'),
                    'nama_tahapan_kegiatan' => $request->input('nama_tahapan_kegiatan'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('tahapan-kegiatan'))->with('success', 'sukses, data tahapan kegiatan berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-tahapan-kegiatan'))->with('error', 'data tahapan kegiatan gagal disimpan');
	    }
    }

    public function delete_tahapan_kegiatan($id)
    {
    	$post = TahapanKegiatan::find($id);
   		if($post->delete()){
   			return redirect(route('tahapan-kegiatan'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('tahapan-kegiatan'))->with('error', 'data gagal dihapus');
   		}
    }
}
