<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UsersKotaKab;
use App\Models\Login;
use App\Models\Kabupaten;

class UsersKotaKabController extends Controller
{
    public function index()
    {
    	$data = UsersKotaKab::get();
    	return view('perencanaan.users-kota-kab-index', compact('data'));
    }

    public function add_users($id=NULL)
    {
    	$users = UsersKotaKab::where('id_login', $id)->first();
    	$kabupaten = Kabupaten::where('status', 1)->get();
      	
        return view('perencanaan.add-users-kota-kab', compact('users', 'kabupaten'));
    }

    public function created_users(Request $request)
    {
    	$id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	$kabName = Kabupaten::where('id', $request->input('id_kabupaten'))->first();
	    	$kabName = !empty($kabName->nama_kabupaten) ? $kabName->nama_kabupaten : "-";

	    	if ($id == NULL OR $id == '') {
	    		$insertLogin = DB::table('login')->insertGetId([
                    'name' => $kabName,
                    'username' => $request->input('username'),
                    'password' => sha1($request->input('password')),
                    'phone' => $request->input('no_telp'),
                    'email' => $request->input('email'),
                    'type' => 1,
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

	    		DB::table('users_kota_kab')->insert([
                    'id_kabupaten' => $request->input('id_kabupaten'),
                    'id_login' => $insertLogin,
                    'email' => $request->input('email'),
                    'no_telp' => $request->input('no_telp'),
                    'alamat' => $request->input('alamat'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('users_kota_kab')->where('id_login', $id)->update([
                    'id_kabupaten' => $request->input('id_kabupaten'),
                    'no_telp' => $request->input('no_telp'),
                    'alamat' => $request->input('alamat'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);

                DB::table('login')->where('id', $id)->update([
                	'name' => $kabName,
                    'username' => $request->input('username'),
                    'phone' => $request->input('no_telp'),
                    'email' => $request->input('email'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('users'))->with('success', 'sukses, data users berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-users'))->with('error', 'data users gagal disimpan');
	    }
    }

    public function delete_users($id)
    {
    	$post1 = UsersKotaKab::where('id_login', $id)->delete();
    	$post2 = Login::where('id', $id)->delete();
   		if($post1 && $post2){
   			return redirect(route('users'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('users'))->with('error', 'data gagal dihapus');
   		}
    }
}
