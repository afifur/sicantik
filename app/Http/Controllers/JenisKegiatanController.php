<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\JenisKegiatan;

class JenisKegiatanController extends Controller
{
    public function index()
    {
    	$data = JenisKegiatan::get();
    	return view('masterdata.jenis-kegiatan-index', compact('data'));
    }

    public function add_jenis_kegiatan($id=NULL)
    {
    	$kegiatan = JenisKegiatan::where('id', $id)->first();
      	
        return view('masterdata.add-jenis-kegiatan', compact('kegiatan'));
    }

    public function created_jenis_kegiatan(Request $request)
    {
    	$validated = $request->validate([
	        'nama_kegiatan' => 'required'
	    ]);

	    $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('jenis_kegiatan')->insert([
                    'kode_kegiatan' => $request->input('kode_kegiatan'),
                    'nama_kegiatan' => $request->input('nama_kegiatan'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('jenis_kegiatan')->where('id', $id)->update([
                    'kode_kegiatan' => $request->input('kode_kegiatan'),
                    'nama_kegiatan' => $request->input('nama_kegiatan'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('jenis-kegiatan'))->with('success', 'sukses, data kegiatan berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-jenis-kegiatan'))->with('error', 'data kegiatan gagal disimpan');
	    }
    }

    public function delete_jenis_kegiatan($id)
    {
    	$post = JenisKegiatan::find($id);
   		if($post->delete()){
   			return redirect(route('jenis-kegiatan'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('jenis-kegiatan'))->with('error', 'data gagal dihapus');
   		}
    }
}
