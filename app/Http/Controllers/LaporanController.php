<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Perencanaan;
use App\Models\Login;
use App\Models\Kabupaten;
use App\Models\Sektor;
use App\Models\JenisKegiatan;
use App\Models\Kategori;
use App\Models\UsersKotaKab;
use App\Models\TahapanKegiatan;
use App\Models\DetailFileKegiatan;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LapDaftarUsulanPaketExport;

class LaporanController extends Controller
{
    public function daftar_usulan_paket()
    {
    	$kabupaten = Kabupaten::where('status', 1)->get();
        $sektor = Sektor::where('status', 1)->get();
        $tahapan_kegiatan = TahapanKegiatan::where('status', 1)->get();

        $data = $this->filter_daftar_usulan_paket();

    	return view('laporan.daftar-usulan-paket-index', compact('data', 'kabupaten', 'sektor', 'tahapan_kegiatan'));
    }

    public function filter_daftar_usulan_paket()
    {
    	$id_sektor = isset($_GET['id_sektor']) ? $_GET['id_sektor'] : "";
        $id_kabupaten = isset($_GET['id_kabupaten']) ? $_GET['id_kabupaten'] : "";
        $periode = isset($_GET['periode']) ? $_GET['periode'] : "";
        $verifikasi = isset($_GET['verifikasi']) ? $_GET['verifikasi'] : "";
        $id_tahapan_kegiatan = isset($_GET['id_tahapan_kegiatan']) ? $_GET['id_tahapan_kegiatan'] : "";
        $status_penilaian = isset($_GET['status_penilaian']) ? $_GET['status_penilaian'] : "";
        
        $dataPerencanaan = Perencanaan::where('status', 1);

        if ($id_sektor != "") {
            $dataPerencanaan->where('id_sektor', $id_sektor);
        }

        if (session()->get('type') == 2) {
            if ($id_kabupaten != "") {
                $dataPerencanaan->where('id_kabupaten', $id_kabupaten);
            }
        }else{
            $dataPerencanaan->where('id_kabupaten', session()->get('id_kabupaten'));
        }
        

        if ($periode != "") {
            $dataPerencanaan->where('periode', $periode);
        }

        if ($id_tahapan_kegiatan != "") {
            $dataPerencanaan->where('id_tahapan_kegiatan', $id_tahapan_kegiatan);
        }

        if ($status_penilaian != "") {
            $dataPerencanaan->where('status_penilaian', $status_penilaian);
        }

        return $dataPerencanaan->get();
    }

    public function export_pdf_usulan_paket()
    {
    	$data = $this->filter_daftar_usulan_paket();

        $pdf = PDF::loadView('laporan.pdf-usulan-paket', ['data' => $data]);
        return $pdf->download(date('Y-m-d').'-laporan-daftar-usulan-paket.pdf');
    }

    public function export_excel_usulan_paket()
    {
    	$data  = $this->filter_daftar_usulan_paket();

        $filename = date('Y-m-d').'-laporan-daftar-usulan-paket.xlsx';
        return Excel::download(new LapDaftarUsulanPaketExport($data), $filename);
    }
}
