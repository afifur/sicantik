<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Sektor;
use App\Models\SettingDetailKegiatan;

class SektorController extends Controller
{
    public function index()
    {
    	$data = Sektor::get();
    	return view('masterdata.sektor-index', compact('data'));
    }

    public function add_sektor($id=NULL)
    {
    	$sektor = Sektor::where('id', $id)->first();
      	
        return view('masterdata.add-sektor', compact('sektor'));
    }

    public function add_setting_document($id=NULL)
    {
        $sektor = Sektor::where('id', $id)->first();
        $setting = SettingDetailKegiatan::where('id_sektor', $id)->get();
        
        return view('masterdata.add-setting-document', compact('sektor', 'setting'));
    }

    public function created_sektor(Request $request)
    {
    	$validated = $request->validate([
	        'nama_sektor' => 'required'
	    ]);

        $id = $request->input('id');

	    DB::beginTransaction();

	    try {

	    	if ($id == NULL OR $id == '') {
	    		DB::table('sektor')->insert([
                    'kode_sektor' => $request->input('kode_sektor'),
                    'nama_sektor' => $request->input('nama_sektor'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);
	    	}else{
	    		DB::table('sektor')->where('id', $id)->update([
                    'kode_sektor' => $request->input('kode_sektor'),
                    'nama_sektor' => $request->input('nama_sektor'),
                    'keterangan' => $request->input('keterangan'),
                    'status' => $request->input('status'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
	    	}

	    	DB::commit();
            return redirect(route('sektor'))->with('success', 'sukses, data sektor berhasil disimpan');
	    } catch (Exception $e) {
	    	DB::rollback();
            return redirect(route('add-sektor'))->with('error', 'data sektor gagal disimpan');
	    }
    }

    public function created_setting_document(Request $request)
    {
        DB::beginTransaction();

        try {
            
            $check = SettingDetailKegiatan::where('id_sektor', $request->input('id_sektor'))->get();
                
            if (count($check) > 0) {
                SettingDetailKegiatan::where('id_sektor', $request->input('id_sektor'))->delete();
            }
            
            if (count($request->input('nama_kegiatan')) > 0) {
                foreach ($request->input('nama_kegiatan') as $key => $value) {
                    DB::table('setting_detail_kegiatan')->insertGetId([
                        'id_sektor' => $request->input('id_sektor'),
                        'nama_kegiatan' => $request->input('nama_kegiatan')[$key],
                        'view_upload' => $request->input('view_upload')[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => session()->get('id'),
                    ]);
                }
            }

            DB::commit();
            return redirect(route('add-setting-document', $request->input('id_sektor')))->with('success', 'sukses, data berhasil ditambah');
        } catch (Exception $e) {
            DB::rollback();
            return redirect(route('add-setting-document', $request->input('id_sektor')))->with('error', 'data gagal ditambah');
        }
        
    }

    public function delete_sektor($id)
    {
    	$post = Sektor::find($id);
   		if($post->delete()){
   			return redirect(route('sektor'))->with('success', 'sukses, data berhasil dihapus');
   		}else{
   			return redirect(route('sektor'))->with('error', 'data gagal dihapus');
   		}
    }
}
