<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Login;

class LoginController extends Controller
{
    public function index()
    {
    	return view('login');
    }

    public function process_login(Request $request)
    {
    	$username = $request->input('username');
        $password = sha1($request->input('password'));

        $validate = Login::where('username', $username)->where('password', $password)->get();

        if (count($validate) > 0) {
            $session_data = [
                'logged_in' => true,
                'id' => $validate[0]->id,
                'name' => $validate[0]->name,
                'email' => $validate[0]->email,
                'phone' => $validate[0]->phone,
                'username' => $validate[0]->username,
                'type' => $validate[0]->type,
                'status' => $validate[0]->status,
                'id_kabupaten' => isset($validate[0]->RefUsersKotaKab->id_kabupaten) ? $validate[0]->RefUsersKotaKab->id_kabupaten : null,
            ];
            $request->session()->put($session_data);
            return redirect('dashboard')->with('success', 'login berhasil, selamat datang');
        }
        
        return redirect('login')->with('error', 'username atau password salah');
    }

    public function logout(Request $request)
    {
        $logout = $request->session()->flush();
        if ($logout == NULL) {
            return redirect('login');
        }
    }
}
