<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Login;

class ProfileController extends Controller
{
    public function index()
    {
    	$id = session()->get('id');
    	$data = Login::where('id', $id)->first();

    	return view('profile.index', compact('data'));
    }

    public function change_password()
    {
    	return view('profile.change-password');
    }

    public function update_profile(Request $request)
    {
    	$validated = $request->validate([
	        'name' => 'required',
	        'username' => 'required',
	    ]);

    	$id = $request->input('id');

    	$save = Login::where('id', $id)->update([
    		'name' => $request->input('name'),
    		'username' => $request->input('username'),
    		'email' => $request->input('email'),
    		'phone' => $request->input('phone')
    	]);

    	if ($save) {
    		return redirect(route('profile'))->with('success', 'sukses, data profile berhasil di update');
    	}else{
    		return redirect(route('profile'))->with('error', 'data profile gagal di update');
    	}
    }

    public function update_password(Request $request)
    {
    	$validated = $request->validate([
	        'old_password' => 'required',
	        'new_password' => 'required',
	        'confirm_password' => 'required|same:new_password'
	    ]);

	    $chck_pass = Login::where('password', sha1($request->input('old_password')))->get();
    	if (count($chck_pass) == 0) {
    		return redirect(route('change-password'))->with('error', 'password lama salah');
    	}

	    $id = $request->input('id');

    	$save = Login::where('id', $id)->update([
    		'password' => sha1($request->input('confirm_password'))
    	]);

    	if ($save) {
    		return redirect(route('change-password'))->with('success', 'sukses, password berhasil diubah');
    	}else{
    		return redirect(route('change-password'))->with('error', 'password gagal diubah');
    	}
    }
}
