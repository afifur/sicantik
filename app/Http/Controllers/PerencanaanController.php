<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Perencanaan;
use App\Models\Login;
use App\Models\Kabupaten;
use App\Models\Sektor;
use App\Models\JenisKegiatan;
use App\Models\Kategori;
use App\Models\TahapanKegiatan;
use App\Models\UsersKotaKab;
use App\Models\DetailFileKegiatan;
use App\Models\Paket;
use App\Models\DetailPaket;

class PerencanaanController extends Controller
{
    public function index()
    {
        $kabupaten = Kabupaten::where('status', 1)->get();
        $sektor = Sektor::where('status', 1)->get();

        if (session()->get('type') == 2) {
            if (isset($_GET['filter']) && $_GET['filter'] == "submit") {
                $data = $this->filter_perencanaan();
            }else{
                $data = Perencanaan::get();
            }
            
        }elseif (session()->get('type') == 1) {
            $data = Perencanaan::where('created_by', session()->get('id'))->get();
        }
    	
    	return view('perencanaan.perencanaan-index', compact('data', 'kabupaten', 'sektor'));
    }

    public function filter_perencanaan()
    {
        $id_sektor = isset($_GET['id_sektor']) ? $_GET['id_sektor'] : "";
        $id_kabupaten = isset($_GET['id_kabupaten']) ? $_GET['id_kabupaten'] : "";
        $periode = isset($_GET['periode']) ? $_GET['periode'] : "";
        $status_penilaian = isset($_GET['status_penilaian']) ? $_GET['status_penilaian'] : "";

        $dataPerencanaan = Perencanaan::where('status', 1);

        if ($id_sektor != "") {
            $dataPerencanaan->where('id_sektor', $id_sektor);
        }

        if ($id_kabupaten != "") {
            $dataPerencanaan->where('id_kabupaten', $id_kabupaten);
        }

        if ($periode != "") {
            $dataPerencanaan->where('periode', $periode);
        }

        if ($status_penilaian != "") {
            $dataPerencanaan->where('status_penilaian', $status_penilaian);
        }

        return $dataPerencanaan->get();
    }

    public function add_perencanaan($id=NULL)
    {
    	$perencanaan = Perencanaan::where('id', $id)->first();
    	$kabupaten = Kabupaten::where('status', 1)->get();
    	$sektor = Sektor::where('status', 1)->get();
    	$kegiatan = JenisKegiatan::where('status', 1)->get();
    	$kategori = Kategori::where('status', 1)->get();
        $tahapan_kegiatan = TahapanKegiatan::where('status', 1)->get();
        $paket = Paket::where('status', 1)->where('id_kabupaten', session()->get('id_kabupaten'))->get();
      	
        return view('perencanaan.add-perencanaan', compact('perencanaan', 'kabupaten', 'sektor', 'kategori', 'kegiatan', 'tahapan_kegiatan', 'paket'));
    }

    public function nilai_perencanaan($id=NULL)
    {
        $perencanaan = Perencanaan::where('id', $id)->first();
        return view('perencanaan.nilai-perencanaan', compact('perencanaan'));
    }

    public function created_perencanaan(Request $request)
    {
    	$id = $request->input('id');

    	DB::beginTransaction();

    	try {
            $nilai_apbn = "";
            if (strlen($request->input('nilai_apbn')) > 4) {
                $nilai_apbn = str_replace(",", "", $request->input('nilai_apbn'));
            }else{
                $nilai_apbn = $request->input('nilai_apbn');
            }

            $resultId = "";

            if ($id == NULL OR $id == '') {
                $insertID = DB::table('perencanaan')->insertGetId([
                    'tgl_perencanaan' => date('Y-m-d'),
                    'id_paket' => $request->input('id_paket'),
                    'nama_paket' => $request->input('nama_paket'),
                    'id_sektor' => $request->input('id_sektor'),
                    'id_kabupaten' => session()->get('id_kabupaten'),
                    'periode' => $request->input('periode'),
                    'id_jenis_kegiatan' => $request->input('id_jenis_kegiatan'),
                    'id_tahapan_kegiatan' => $request->input('id_tahapan_kegiatan'),
                    'id_kategori' => $request->input('id_kategori'),
                    'lokasi' => $request->input('lokasi'),
                    'volume' => $request->input('volume'),
                    'satuan' => $request->input('satuan'),
                    'nilai_apbn' => $nilai_apbn * 1000,
                    'keterangan' => $request->input('keterangan'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

                if (count($request->input('nama_kegiatan')) > 0) {
                    foreach ($request->input('nama_kegiatan') as $key => $value) {
                        $idDetail = DB::table('detail_file_kegiatan')->insertGetId([
                            'perencanaan_id' => $insertID,
                            'id_paket' => $request->input('id_paket'),
                            'id_detail_paket' => $request->input('id_detail_paket')[$key],
                            'nama_kegiatan' => $request->input('nama_kegiatan')[$key],
                            'status_dokumen' => $request->input('status_dokumen')[$key],
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => session()->get('id'),
                        ]);

                        $files = $request->file('file_dokumen');

                        $this->upload_file($files, $request, $insertID, $idDetail, $key);
                    }
                }

                $resultId = $insertID;

            }else{
                $check = Perencanaan::where('id', $id)->where('nilai_apbn', $nilai_apbn)->first();

                $nilai_apbns = "";
                if ($check == NULL) {
                    $nilai_apbns = $nilai_apbn * 1000;
                }else{
                    $nilai_apbns = $nilai_apbn;
                }
                
                DB::table('perencanaan')->where('id', $id)->update([
                    'id_paket' => $request->input('id_paket'),
                    'nama_paket' => $request->input('nama_paket'),
                    'id_sektor' => $request->input('id_sektor'),
                    'id_kabupaten' => session()->get('id_kabupaten'),
                    'periode' => $request->input('periode'),
                    'id_jenis_kegiatan' => $request->input('id_jenis_kegiatan'),
                    'id_tahapan_kegiatan' => $request->input('id_tahapan_kegiatan'),
                    'id_kategori' => $request->input('id_kategori'),
                    'lokasi' => $request->input('lokasi'),
                    'volume' => $request->input('volume'),
                    'satuan' => $request->input('satuan'),
                    'nilai_apbn' => $nilai_apbns,
                    'keterangan' => $request->input('keterangan'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

                if (count($request->input('nama_kegiatan')) > 0) {
                    foreach ($request->input('nama_kegiatan') as $key => $value) {
                        $idDetail = $request->input('id_detail')[$key];

                        $check = DetailFileKegiatan::where('id', $idDetail)->first();
                        DB::table('detail_file_kegiatan')->where('id', $idDetail)->update([
                            'nama_kegiatan' => $request->input('nama_kegiatan')[$key],
                            'status_dokumen' => $request->input('status_dokumen')[$key],
                            'updated_at' => date('Y-m-d H:i:s'),
                            'updated_by' => session()->get('id'),
                        ]);
                    }
                }

                $resultId = $id;
                
            }
            
            DB::commit();
            return redirect(route('edit-perencanaan', $resultId))->with('success', 'sukses, data perencanaan berhasil disimpan');
    	} catch (Exception $e) {
    		DB::rollback();
            return redirect(route('add-perencanaan'))->with('error', 'data perencanaan gagal disimpan');
    	}
    }

    public function upload_file($files, $request, $id, $idDetail, $key)
    {
        if (isset($files[$key])) {
        	$path = public_path('uploads/file/'. $id);
            if (!is_dir($path)) {
                $createPath = File::makeDirectory($path, $mode = 0777, true, true);
            }

            if ($request->hasFile('file_dokumen')) {
                $imageFile = $files[$key]->getClientOriginalName();


                $createdFile = $files[$key]->move($path, $imageFile);

                if ($createdFile) {
                    $created_data = DetailFileKegiatan::where('id', $idDetail)->update([
                        'file_dokumen' => $imageFile,
                        'status_dokumen' => $request->input('status_dokumen')[$key],
                    ]);
                }
            }
        }
    }

    public function save_upload_file(Request $request)
    {
        $idDetail = $request->input('id_detailx')[0];
        $id = $request->input('id_perencanaanx');

        $path = public_path('uploads/file/'. $id);
        if (!is_dir($path)) {
            $createPath = File::makeDirectory($path, $mode = 0777, true, true);
        }



        if ($request->hasFile('file_dokumen')) {
            $imageFile = $request->file('file_dokumen')[0]->getClientOriginalName();
            $createdFile = $request->file('file_dokumen')[0]->move($path, $imageFile);

            if ($createdFile) {
                $created_data = DetailFileKegiatan::where('id', $idDetail)->update([
                    'file_dokumen' => $imageFile,
                ]);
            }
        }

        return redirect(route('edit-perencanaan', $id))->with('success', 'sukses, data perencanaan berhasil disimpan');
    }

    public function update_penilaian_perencanaan(Request $request)
    {
        $id_perencanaan = $request->input('id');

        DB::beginTransaction();

        try {
            if (count($request->input('id_detail')) > 0) {
                $checkNilai = 0;
                foreach ($request->input('id_detail') as $keyx => $valuex) {
                    $score_dokumenx = 0;
                    if ($request->input('penilaian_per_doc')[$keyx] == 1) {
                        $score_dokumenx = $request->input('score_dokumen')[$keyx];
                    }
                    $checkNilai += $score_dokumenx;
                }

                if ($checkNilai > 100) {
                    return redirect(route('nilai-perencanaan', $id_perencanaan))->with('error', 'score tidak boleh lebih dari 100');
                }

                foreach ($request->input('id_detail') as $key => $value) {
                    $idx = $request->input('id_detail')[$key];

                    $score_dokumens = 0;
                    if ($request->input('penilaian_per_doc')[$key] == 1) {
                        $score_dokumens = $request->input('score_dokumen')[$key];
                    }

                    DB::table('detail_file_kegiatan')->where('id', $idx)->update([
                        'penilaian_per_doc' => $request->input('penilaian_per_doc')[$key],
                        'score_dokumen' => $score_dokumens,
                        'catatan' => $request->input('catatan')[$key],
                        'updated_at' => date('Y-m-d H:i:s'),
                        'updated_by' => session()->get('id'),
                    ]);
                }
            }

            $sum = DetailFileKegiatan::where('perencanaan_id', $id_perencanaan)->sum('score_dokumen');

            if ($sum < 100) {
                DB::table('perencanaan')->where('id', $id_perencanaan)->update([
                    'status_penilaian' => 0,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
            }else{
                DB::table('perencanaan')->where('id', $id_perencanaan)->update([
                    'status_penilaian' => 1,
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => session()->get('id'),
                ]);
            }

            DB::commit();
            return redirect(route('nilai-perencanaan', $id_perencanaan))->with('success', 'sukses, data perencanaan berhasil diupdate');
        } catch (Exception $e) {
            DB::rollback();
            return redirect(route('nilai-perencanaan', $id_perencanaan))->with('error', 'data perencanaan gagal disimpan');
        }
    }

    public function delete_perencanaan($id)
    {
        $post1 = Perencanaan::where('id', $id)->delete();
        $post2 = DetailFileKegiatan::where('perencanaan_id', $id)->delete();
        if($post1 && $post2){
            return redirect(route('perencanaan'))->with('success', 'sukses, data berhasil dihapus');
        }else{
            return redirect(route('perencanaan'))->with('error', 'data gagal dihapus');
        }
    }

    public function delete_file($id, $id_perencanaan)
    {

        $delete = DetailFileKegiatan::where('id', $id)->update([
            'file_dokumen' => null,
            'score_dokumen' => 0
        ]);

        if($delete){
            return redirect(route('edit-perencanaan', $id_perencanaan))->with('success', 'sukses, file berhasil dihapus');
        }else{
            return redirect(route('edit-perencanaan', $id_perencanaan))->with('error', 'file gagal dihapus');
        }
    }
}
