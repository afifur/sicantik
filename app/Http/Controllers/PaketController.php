<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Models\Paket;
use App\Models\DetailPaket;
use App\Models\Sektor;
use App\Models\JenisKegiatan;
use App\Models\Kabupaten;

class PaketController extends Controller
{
    public function index()
    {
        $data = Paket::get();
     	
    	return view('perencanaan.paket-index', compact('data'));
    }

    public function add_paket($id=NULL)
    {
    	$paket = Paket::where('id', $id)->first();
    	$sektor = Sektor::where('status', 1)->get();
    	$kegiatan = JenisKegiatan::where('status', 1)->get();
        $kabupaten = Kabupaten::where('status', 1)->get();
      	
        return view('perencanaan.add-paket', compact('paket', 'sektor', 'kegiatan', 'kabupaten'));
    }

    public function created_paket(Request $request)
    {
    	$id = $request->input('id');

    	DB::beginTransaction();

    	try {
        	
            if ($id == NULL OR $id == '') {
                $insertID = DB::table('paket')->insertGetId([
                    'kode_paket' => $request->input('kode_paket'),
                    'nama_paket' => $request->input('nama_paket'),
                    'keterangan' => $request->input('keterangan'),
                    'id_sektor' => $request->input('id_sektor'),
                    'id_jenis_kegiatan' => $request->input('id_jenis_kegiatan'),
                    'id_kabupaten' => $request->input('id_kabupaten'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

                if (count($request->input('nama_kegiatan')) > 0) {
	                foreach ($request->input('nama_kegiatan') as $key => $value) {
	                    DB::table('detail_paket')->insertGetId([
	                        'paket_id' => $insertID,
	                        'nama_kegiatan' => $request->input('nama_kegiatan')[$key],
	                        'view_upload' => $request->input('view_upload')[$key],
	                        'created_at' => date('Y-m-d H:i:s'),
	                        'created_by' => session()->get('id'),
	                    ]);
	                }
	            }

            }else{
            	DB::table('paket')->where('id', $id)->update([
                    'kode_paket' => $request->input('kode_paket'),
                    'nama_paket' => $request->input('nama_paket'),
                    'keterangan' => $request->input('keterangan'),
                    'id_sektor' => $request->input('id_sektor'),
                    'id_jenis_kegiatan' => $request->input('id_jenis_kegiatan'),
                    'id_kabupaten' => $request->input('id_kabupaten'),
                    'status' => $request->input('status'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => session()->get('id'),
                ]);

                $check = DetailPaket::where('paket_id', $id)->get();
                
	            if (count($check) > 0) {
	                DetailPaket::where('paket_id', $id)->delete();
	            }
	            
	            if (count($request->input('nama_kegiatan')) > 0) {
	                foreach ($request->input('nama_kegiatan') as $key => $value) {
	                    DB::table('detail_paket')->insertGetId([
	                        'paket_id' => $id,
	                        'nama_kegiatan' => $request->input('nama_kegiatan')[$key],
	                        'view_upload' => $request->input('view_upload')[$key],
	                        'created_at' => date('Y-m-d H:i:s'),
	                        'created_by' => session()->get('id'),
	                    ]);
	                }
	            }
            }
                
            
            DB::commit();
            return redirect(route('paket'))->with('success', 'sukses, data paket berhasil disimpan');
    	} catch (Exception $e) {
    		DB::rollback();
            return redirect(route('add-paket'))->with('error', 'data paket gagal disimpan');
    	}
    }

    public function delete_paket($id)
    {
        $post1 = Paket::where('id', $id)->delete();
        $post2 = DetailPaket::where('paket_id', $id)->delete();
        if($post1 && $post2){
            return redirect(route('paket'))->with('success', 'sukses, data berhasil dihapus');
        }else{
            return redirect(route('paket'))->with('error', 'data gagal dihapus');
        }
    }

    public function get_file_document_per_paket(Request $request)
    {
        $id = $request->input('id_paket');
        $data = Paket::where('id', $id)->first();
        $detail = DetailPaket::where('paket_id', $id)->get();

        $sektor = $data->RefSektor->nama_sektor;
        $jenis_kegiatan = $data->RefJenisKegiatan->nama_kegiatan;

        $result = "";
        if (!empty($data) && count($detail) > 0) {
            $result = array(
                'paket' => $data,
                'nama_sektor' => $sektor,
                'nama_kegiatan' => $jenis_kegiatan,
                'detail_paket' => $detail
            );
        }else{
            $result = [];
        }

        echo json_encode($result);        
    }
}
