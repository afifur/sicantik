<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Perencanaan;
use App\Models\Sektor;
use App\Models\UsersKotaKab;
use App\Models\Content;

class DashboardController extends Controller
{
    public function index()
    {
    	$profiles = Profile::where('id', 1)->first();
    	$totalUsers = UsersKotaKab::count('*');
    	$totalSektor = Sektor::count('*');
    	$perencanaanVerifikasi = Perencanaan::where('status_penilaian', 1)->count('*');
    	$perencanaanNotVerifikasi = Perencanaan::where('status_penilaian', 0)->count('*');
    	$contents = Content::where('type_content', 1)->get();

    	return view('dashboard', compact('profiles', 'totalUsers', 'totalSektor', 'perencanaanVerifikasi', 'perencanaanNotVerifikasi', 'contents'));
    }
}
