<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sektor extends Model
{
    use HasFactory;

    protected $table = 'sektor';

    protected $fillable = [
    	"id",
    	"kode_sektor",
        "nama_sektor",
    	"keterangan",
    	"status",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefSettingDetailKegiatan()
    {
        return $this->hasMany('App\Models\SettingDetailKegiatan');
    }

    public static function get_status_name($status)
    {
        $result = "";
        if ($status == 0) {
            $result = "<label class='label label-danger'><i class='fa fa-remove'></i> Non Aktif</label>";
        }elseif ($status == 1) {
            $result = "<label class='label label-success'><i class='fa fa-check'></i> Aktif</label>";
        }

        return $result;
    }
}
