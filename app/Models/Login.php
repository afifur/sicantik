<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    use HasFactory;

    protected $table = 'login';

    protected $fillable = [
    	"id",
    	"name",
        "email",
        "phone",
    	"username",
    	"password",
    	"type",
    	"status",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
        return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUsersKotaKab()
    {
        return $this->hasOne('App\Models\UsersKotaKab', 'id_login' , 'id');
    }

    public static function get_status_name($status)
    {
        $result = "";
        if ($status == 0) {
            $result = "<label class='label label-danger'><i class='fa fa-remove'></i> Non Aktif</label>";
        }elseif ($status == 1) {
            $result = "<label class='label label-success'><i class='fa fa-check'></i> Aktif</label>";
        }

        return $result;
    }

    public function get_type_name($type)
    {
    	$result = "";
        if ($type == 1) {
            $result = "Admin Kab/Kota";
        }elseif ($type == 2) {
            $result = "Admin Balai";
        }

        return $result;
    }
}
