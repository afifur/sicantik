<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingDetailKegiatan extends Model
{
    use HasFactory;

    protected $table = 'setting_detail_kegiatan';

    protected $fillable = [
    	"id",
    	"id_sektor",
    	"nama_kegiatan",
    	"view_upload",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefSektor()
    {
    	return $this->hasOne('App\Models\Sektor', 'id' , 'id_sektor');
    }
}
