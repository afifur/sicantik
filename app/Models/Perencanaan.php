<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perencanaan extends Model
{
    use HasFactory;

    protected $table = 'perencanaan';

    protected $fillable = [
    	"id",
    	"kode_perencanaan",
        "tgl_perencanaan",
        "id_paket",
    	"nama_paket",
    	"id_sektor",
    	"id_kabupaten",
    	"periode",
        "id_jenis_kegiatan",
        "id_tahapan_kegiatan",
    	"id_kategori",
    	"lokasi",
    	"volume",
    	"satuan",
        "nilai_apbn",
    	"score_all",
    	"keterangan",
    	"status_penilaian",
        "status",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefSektor()
    {
    	return $this->hasOne('App\Models\Sektor', 'id' , 'id_sektor');
    }

    public function RefPaket()
    {
        return $this->hasOne('App\Models\Paket', 'id' , 'id_paket');
    }

    public function RefKabupaten()
    {
    	return $this->hasOne('App\Models\Kabupaten', 'id' , 'id_kabupaten');
    }

    public function RefJenisKegiatan()
    {
    	return $this->hasOne('App\Models\JenisKegiatan', 'id' , 'id_jenis_kegiatan');
    }

    public function RefTahapanKegiatan()
    {
        return $this->hasOne('App\Models\TahapanKegiatan', 'id' , 'id_tahapan_kegiatan');
    }

    public function RefKategori()
    {
    	return $this->hasOne('App\Models\Kategori', 'id' , 'id_kategori');
    }

    public function RefDetailKegiatan()
    {
        return $this->hasMany('App\Models\DetailFileKegiatan');
    }

    public static function get_status_penilaian_name($status)
    {
        $result = "";
        if ($status == 0) {
            $result = "<label class='label label-danger'><i class='fa fa-warning'></i> Belum dinilai</label>";
        }elseif ($status == 1) {
            $result = "<label class='label label-success'><i class='fa fa-check'></i> Sudah dinilai</label>";
        }

        return $result;
    }

    public static function update_score_plus($id, $new_score)
    {
        $old_score = Perencanaan::where('id', $id)->first();
        $updated = Perencanaan::where('id', $id)->update([
            'score_all' => $old_score->score_all + $new_score,
        ]);
    }

    public static function update_score_min($id, $new_score)
    {
        $old_score = Perencanaan::where('id', $id)->first();
        if ($old_score->score_all > 0) {
            $updated = Perencanaan::where('id', $id)->update([
                'score_all' => $old_score->score_all - $new_score,
            ]);
        }
    }
}
