<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    use HasFactory;

    protected $table = 'kabupaten';

    protected $fillable = [
    	"id",
    	"provinsi_id",
        "nama_kabupaten",
    	"status",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefProvinsi()
    {
    	return $this->hasOne('App\Models\Provinsi', 'id' , 'provinsi_id');
    }

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public static function get_status_name($status)
    {
        $result = "";
        if ($status == 0) {
            $result = "<label class='label label-danger'><i class='fa fa-remove'></i> Non Aktif</label>";
        }elseif ($status == 1) {
            $result = "<label class='label label-success'><i class='fa fa-check'></i> Aktif</label>";
        }

        return $result;
    }
}
