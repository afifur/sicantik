<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPaket extends Model
{
    use HasFactory;

    protected $table = 'detail_paket';

    protected $fillable = [
    	"id",
    	"paket_id",
        "nama_kegiatan",
    	"view_upload",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefPaket()
    {
    	return $this->hasOne('App\Models\Paket', 'id' , 'paket_id');
    }
}
