<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailFileKegiatan extends Model
{
    use HasFactory;

    protected $table = 'detail_file_kegiatan';

    protected $fillable = [
    	"id",
    	"perencanaan_id",
        "id_paket",
    	"id_detail_paket",
    	"nama_kegiatan",
    	"file_dokumen",
    	"status_dokumen",
    	"score_dokumen",
    	"penilaian_per_doc",
    	"catatan",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefPerencanaan()
    {
    	return $this->hasOne('App\Models\Perencanaan', 'id' , 'perencanaan_id');
    }

    public function RefPaket()
    {
        return $this->hasOne('App\Models\Paket', 'id' , 'id_paket');
    }

    public function RefDetailPaket()
    {
        return $this->hasOne('App\Models\DetailPaket', 'id' , 'id_detail_paket');
    }

    public function RefSettingKegiatan()
    {
    	return $this->hasOne('App\Models\SettingKegiatan', 'id' , 'id_setting_kegiatan');
    }
}
