<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersKotaKab extends Model
{
    use HasFactory;

    protected $table = 'users_kota_kab';

    protected $fillable = [
    	"id",
    	"id_kabupaten",
        "id_login",
    	"email",
    	"no_telp",
    	"alamat",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefKotaKab()
    {
    	return $this->hasOne('App\Models\Kabupaten', 'id' , 'id_kabupaten');
    }

    public function RefLogin()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'id_login');
    }
}
