<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    use HasFactory;

    protected $table = 'paket';

    protected $fillable = [
    	"id",
    	"kode_paket",
        "nama_paket",
    	"keterangan",
    	"id_sektor",
    	"id_jenis_kegiatan",
        "id_kabupaten",
    	"status",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }

    public function RefSektor()
    {
    	return $this->hasOne('App\Models\Sektor', 'id' , 'id_sektor');
    }

    public function RefJenisKegiatan()
    {
    	return $this->hasOne('App\Models\JenisKegiatan', 'id' , 'id_jenis_kegiatan');
    }

    public function RefKabupaten()
    {
        return $this->hasOne('App\Models\Kabupaten', 'id' , 'id_kabupaten');
    }

    public function RefDetailPaket()
    {
    	return $this->hasMany('App\Models\DetailPaket');
    }
}
