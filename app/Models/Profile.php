<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profile';

    protected $fillable = [
    	"id",
    	"title",
        "deskripsi",
    	"nama_kepala_balai1",
    	"jabatan_kepala_balai1",
    	"foto_kepala_balai1",
    	"nama_kepala_balai2",
        "jabatan_kepala_balai2",
    	"foto_kepala_balai2",
    	"nama_kasubag",
    	"jabatan_kasubag",
    	"foto_kasubag",
        "nama_kasie_wil1",
    	"jabatan_kasie_wil1",
    	"foto_kasie_wil1",
    	"nama_kasie_wil2",
    	"jabatan_kasie_wil2",
        "foto_kasie_wil2",
    	"nama_kasatker",
    	"jabatan_kasatker",
    	"foto_kasatker",
    	"created_at",
    	"updated_at",
    	"created_by",
    	"updated_by"
    ];

    public function RefCreatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'created_by');
    }

    public function RefUpdatedBy()
    {
    	return $this->hasOne('App\Models\Login', 'id' , 'updated_by');
    }
}
