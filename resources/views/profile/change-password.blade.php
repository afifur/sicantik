@extends('app')
@section('title', '- Ganti Password')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('change-password') }}">Ganti Password</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-green-sharp">
			<i class="fa fa-lock font-green-sharp"></i>
			<span class="caption-subject bold uppercase"> Ganti Password</span>
		</div>
		<div class="actions">
			<button type="button" id="btn-submit-act" class="btn btn-circle green"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

		<form role="form" id="form-submit-password" action="{{ route('update-password') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="form-group">
					<label>Password lama</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="hidden" name="id" id="id" value="{{ session()->get('id') }}">
						<input type="password" name="old_password" id="old_password" class="form-control" placeholder="Masukan password lama">
					</div>
				</div>
				<div class="form-group">
					<label>Password Baru</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" name="new_password" id="new_password" class="form-control" placeholder="Masukan password baru">
					</div>
				</div>
				<div class="form-group">
					<label>Konfirmasi Password Baru</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Konfirmasi password baru">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		$("#form-submit-password").submit();
	});
</script>
@endsection