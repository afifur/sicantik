@extends('app')
@section('title', '- Profile')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('profile') }}">Profile</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-green-sharp">
			<i class="fa fa-users font-green-sharp"></i> 
			<span class="caption-subject bold uppercase"> Profile</span>
			<span class="caption-helper">form user profile</span>
		</div>
		<div class="actions">
			<button type="button" id="btn-submit-act" class="btn btn-circle green"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

		<form role="form" id="form-submit-profile" action="{{ route('update-profile') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-users"></i></span>
								<input type="hidden" name="id" id="id" value="{{ !empty($data->id) ? $data->id : '' }}">
								<input type="text" name="name" id="name" value="{{ !empty($data->name) ? $data->name : '' }}" class="form-control" placeholder="Name">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Username</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-users"></i></span>
								<input type="text" name="username" id="username" value="{{ !empty($data->username) ? $data->username : '' }}" class="form-control" placeholder="Username">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Email</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="text" name="email" id="email" value="{{ !empty($data->email) ? $data->email : '' }}" class="form-control" placeholder="Email">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Nomor HP</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone"></i></span>
								<input type="text" name="phone" id="phone" value="{{ !empty($data->phone) ? $data->phone : '' }}" class="form-control" placeholder="Nomor HP">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		$("#form-submit-profile").submit();
	});
</script>
@endsection