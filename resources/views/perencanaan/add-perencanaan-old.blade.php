@extends('app')
@section('title', '-'.empty($perencanaan->id) ? 'Tambah':'Edit'.' Perencanaan')

@section('css')
<style type="text/css">
	.visuallyhidden{
		border: 0;
	    clip: rect(0 0 0 0);
	    height: 1px;
	    margin: -1px;
	    overflow: hidden;
	    padding: 0;
	    position: absolute;
	    width: 1px;
	}
</style>
@endsection

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('perencanaan') }}">Perencanaan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-perencanaan') }}">@php echo empty($perencanaan->id) ? 'Tambah':'Edit' @endphp Perencanaan</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($perencanaan->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($perencanaan->id) ? 'Tambah':'Edit' @endphp Perencanaan</span>
		</div>
		<div class="actions">
			<a href="{{ route('perencanaan') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			@if(session()->get('type') == 1)
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			@endif
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-perencanaan" action="{{ route('create-perencanaan') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<input type="hidden" name="id" id="id" value="{{ !empty($perencanaan->id) ? $perencanaan->id : '' }}">
							<label>Paket</label>
							<select class="form-control select2me" data-placeholder="pilih paket" name="id_paket" id="id_paket" onchange="getDetailDocument()">
								<option value=""></option>
								@if(count($paket) > 0)
									@foreach($paket as $paket)
									@php
										$selectedPaket = "";
										if(!empty($perencanaan->id_paket) && $perencanaan->id_paket == $paket->id){
											$selectedPaket = "selected";
										}else if(old('id_paket') == $paket->id){
											$selectedPaket = "selected";
										}
									@endphp
									<option value="{{ $paket->id }}" {{ $selectedPaket }}>{{ $paket->nama_paket }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="hidden" name="id" id="id" value="{{ !empty($perencanaan->id) ? $perencanaan->id : '' }}">
							<label>Sektor</label>
							<select class="form-control select2me" data-placeholder="pilih sektor" name="id_sektor" id="id_sektor">
								<option value=""></option>
								@if(count($sektor) > 0)
									@foreach($sektor as $sektor)
									@php
										$selectedSektor = "";
										if(!empty($perencanaan->id_sektor) && $perencanaan->id_sektor == $sektor->id){
											$selectedSektor = "selected";
										}else if(old('id_sektor') == $sektor->id){
											$selectedSektor = "selected";
										}
									@endphp
									<option value="{{ $sektor->id }}" {{ $selectedSektor }}>{{ $sektor->nama_sektor }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Jenis Kegiatan</label>
							<select class="form-control select2me" data-placeholder="pilih jenis kegiatan" name="id_jenis_kegiatan" id="id_jenis_kegiatan">
								<option value=""></option>
								@if(count($kegiatan) > 0)
									@foreach($kegiatan as $kegiatan)
									@php
										$selectedKegiatan = "";
										if(!empty($perencanaan->id_jenis_kegiatan) && $perencanaan->id_jenis_kegiatan == $kegiatan->id){
											$selectedKegiatan = "selected";
										}else if(old('id_jenis_kegiatan') == $kegiatan->id){
											$selectedKegiatan = "selected";
										}
									@endphp
									<option value="{{ $kegiatan->id }}" {{ $selectedKegiatan }}>{{ $kegiatan->nama_kegiatan }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Kategori</label>
							<select class="form-control select2me" data-placeholder="pilih kategori" name="id_kategori" id="id_kategori">
								<option value=""></option>
								@if(count($kategori) > 0)
									@foreach($kategori as $kategori)
									@php
										$selectedKategori = "";
										if(!empty($perencanaan->id_kategori) && $perencanaan->id_kategori == $kategori->id){
											$selectedKategori = "selected";
										}else if(old('id_kategori') == $kategori->id){
											$selectedKategori = "selected";
										}
									@endphp
									<option value="{{ $kategori->id }}" {{ $selectedKategori }}>{{ $kategori->nama_kategori }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Tahapan Kegiatan</label>
							<select class="form-control select2me" data-placeholder="pilih tahapan kegiatan" name="id_tahapan_kegiatan" id="id_tahapan_kegiatan">
								<option value=""></option>
								@if(count($tahapan_kegiatan) > 0)
									@foreach($tahapan_kegiatan as $tahapan_kegiatan)
									@php
										$selectedTahapanKegiatan = "";
										if(!empty($perencanaan->id_tahapan_kegiatan) && $perencanaan->id_tahapan_kegiatan == $tahapan_kegiatan->id){
											$selectedTahapanKegiatan = "selected";
										}else if(old('id_tahapan_kegiatan') == $tahapan_kegiatan->id){
											$selectedTahapanKegiatan = "selected";
										}
									@endphp
									<option value="{{ $tahapan_kegiatan->id }}" {{ $selectedTahapanKegiatan }}>{{ $tahapan_kegiatan->nama_tahapan_kegiatan }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama Paket</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="nama_paket" id="nama_paket" value="{{ !empty($perencanaan->nama_paket) ? $perencanaan->nama_paket : old('nama_paket') }}" class="form-control" placeholder="nama paket">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Lokasi</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="lokasi" id="lokasi" value="{{ !empty($perencanaan->lokasi) ? $perencanaan->lokasi : old('lokasi') }}" class="form-control" placeholder="lokasi">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Volume</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="volume" id="volume" value="{{ !empty($perencanaan->volume) ? $perencanaan->volume : old('volume') }}" class="form-control" placeholder="volume">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Satuan</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="satuan" id="satuan" value="{{ !empty($perencanaan->satuan) ? $perencanaan->satuan : old('satuan') }}" class="form-control" placeholder="satuan">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Nilai APBN (dalam ribuan)</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="nilai_apbn" id="nilai_apbn" value="{{ !empty($perencanaan->nilai_apbn) ? number_format($perencanaan->nilai_apbn) : old('nilai_apbn') }}" class="form-control format-number" placeholder="nilai apbn">
							</div>
						</div>
						<div class="form-group">
							<label>Periode</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="periode" id="periode" value="{{ !empty($perencanaan->periode) ? $perencanaan->periode : old('periode') }}" class="form-control" placeholder="periode">
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label>Keterangan</label>
							<textarea name="keterangan" id="keterangan" rows="4" class="form-control" placeholder="keterangan">{{ !empty($perencanaan->keterangan) ? $perencanaan->keterangan : old('keterangan') }}</textarea>
						</div>
					</div>
				</div> <br>
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab1" data-toggle="tab"><i class="fa fa-file"></i> Upload Dokumen</a>
						</li>
					</ul>
					<div class="tab-content no-space">
						<div class="tab-pane active" id="tab1"><br>
							<h4 align="center">Readiness Criteria</h4> <br>
							<div id="result-file"></div>
							@if(!empty($perencanaan->RefDetailKegiatan))
								<table class="table table-hover table-bordered">
									<tr style="font-family:calibri;">
										<th width="50">No</th>
										<th>Nama Dokumen</th>
										<th width="150">Status</th>
										<th width="100">File</th>
										<th width="150">Status Penilaian</th>
										<th width="100">Score</th>
										<th width="80">Aksi</th>
									</tr>
									@foreach($perencanaan->RefDetailKegiatan as $detail)
									<tr style="font-family:calibri;">
										<td>{{ $loop->iteration }}</td>
										<td>
											{{ $detail->nama_kegiatan }}
											<input type="hidden" name="id_detail[]" id="id_detail" value="{{ $detail->id }}">
											<input type="hidden" name="nama_kegiatan[]" id="nama_kegiatan[]" value="{{ $detail->nama_kegiatan }}">
										</td>
										<td>
											<select class="form-control select2me" name="status_dokumen[]" id="status_dokumen">
												@php
													$selectedYA = "";
													if(isset($detail->status_dokumen) && $detail->status_dokumen == "1"){
														$selectedYA = "selected";
													}

													$selectedTIDAK = "";
													if(isset($detail->status_dokumen) && $detail->status_dokumen == "0"){
														$selectedTIDAK = "selected";
													}
												@endphp
												<option value="1" {{ $selectedYA }}>YA</option>
												<option value="0" {{ $selectedTIDAK }}>TIDAK</option>
											</select>
										</td>
										<td>
											@php
												$gets = App\Models\DetailPaket::where('id', $detail->id_detail_paket)->first();
											@endphp

											@if($detail->file_dokumen != null)
											<a href="{{url('uploads/file/'.$perencanaan->id.'/'.$detail->file_dokumen)}}" download>{{ $detail->file_dokumen }} <i class="fa fa-download"></i></a>
											@elseif((!empty($gets->view_upload) && $gets->view_upload == 1) && $detail->file_dokumen == null)
											<input type="file" name="file_dokumen[]" id="file_dokumen" />
											@endif

											@if($detail->penilaian_per_doc == 2)
											<input type="file" name="file_dokumen[]" id="file_dokumen" />
											@else
											<input type="file" name="file_dokumen[]" id="file_dokumen" class="visuallyhidden" />
											@endif

										</td>
										<td>
											@if($detail->penilaian_per_doc == 1)
											<label class="label label-success"><i class="fa fa-check"></i> OK</label>
											@elseif($detail->penilaian_per_doc == null)
											<label class="label label-warning"><i class="fa fa-warning"></i> Belum Dinilai</label>
											@elseif($detail->penilaian_per_doc == 2)
											<label class="label label-danger"><i class="fa fa-remove"></i> Revisi</label>
											@endif

											<div style="margin-top:10px;">
												@if(isset($detail->penilaian_per_doc) && $detail->penilaian_per_doc == 2)
												<b>catatan :</b> <br>
												<p style="line-height:10px; font-size:13px;">{{ $detail->catatan }}</p>
												@endif
											</div>
										</td>
										<td>
											{{ $detail->score_dokumen }}
										</td>
										<td>
											@if($detail->file_dokumen != null)
											<a href="#modal-view-file" data-toggle="modal" data-name="{{ $detail->nama_kegiatan }}" data-url="{{url('uploads/file/'.$perencanaan->id.'/'.$detail->file_dokumen)}}" onclick="getFile(this)" class="btn btn-sm blue"><i class="fa fa-eye"></i></a>
											<a href="{{ route('delete-file', [$detail->id, !empty($perencanaan->id) ? $perencanaan->id : '' ]) }}" onclick="return confirm('ingin menghapus file ini ?')" class="btn red btn-sm"><i class="fa fa-trash"></i></a>
											@endif
										</td>
									</tr>
									@endforeach
								</table>
							@endif
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="modal-view-file" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lihat File <span id="title-header"></span></h4>
			</div>
			<div class="modal-body">
				<iframe id="view_file" width="100%;" height="500px;"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-circle default">Close</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#id_sektor").val() == "") {
			alert("sektor belum dipilih");
		}else if($("#id_jenis_kegiatan").val() == ""){
			alert("jenis kegiatan belum dipilih");
		}else if($("#id_kategori").val() == ""){
			alert("kategori belum dipilih");
		}else if($("#nama_paket").val() == ""){
			alert("nama paket tidak boleh kosong");
		}else if($("#status").val() == ""){
			alert("status belum dipilih");
		}else{
			$("#form-submit-perencanaan").submit();
		}
		
	});

	function getFile (element) {
		var url = $(element).data('url');
		var name = $(element).data('name');
		$("#view_file").attr("src", url);
		$("#title-header").text(name);
	}

	function getDetailDocument () {
		var id_paket = $("#id_paket").val();

		if (id_paket != "") {
			var post = {
				"_token": "{{ csrf_token() }}",
				"id_paket": id_paket,
			}

			$.ajax({
				url: "{{ route('get-file-document-per-paket') }}",
				type: "POST",
				dataType: "JSON",
				data: post,
				beforeSend:function(){
					$("#result-file").html("<img src='{{url('assets/global/img/loading-spinner-grey.gif')}}' alt=' class='loading'><span>&nbsp;&nbsp;Loading... </span>");
				},
				success: function(output){
					if (output.detail_paket.length > 0) {
						var res = ""
						+"<table class='table table-hover table-bordered'>"
							+"<tr style='font-family:calibri;'>"
								+"<th>No</th>"
								+"<th>Nama Dokumen</th>"
								+"<th width='200'>Status</th>"
								+"<th width='100'>File</th>"
							+"</tr>"
							var number = 1;
							for (var i = 0; i < output.detail_paket.length; i++) {
								res +=
								"<tr style='font-family:calibri;'>"
									+"<td>"+(number++)+"<input type='hidden' name='id_detail_paket[]' value="+output.detail_paket[i].id+"></td>"
									+"<td>"+output.detail_paket[i].nama_kegiatan+"<input type='hidden' name='nama_kegiatan[]' value="+output.detail_paket[i].nama_kegiatan+"></td>"
									+"<td>"
										+"<select class='form-control select2me' name='status_dokumen[]' id='status_dokumen"+number+"'>"
											+"<option value='1'>YA</option>"
											+"<option value='0'}>TIDAK</option>"
										+"</select>"
									+"</td>"
									+"<td>"
										if (output.detail_paket[i].view_upload == 1) {
											res += "<input type='file' name='file_dokumen[]' id='file_dokumen"+number+"' />"
										}else{
											res += "<input type='file' name='file_dokumen[]' class='visuallyhidden' />"
										}
									res +="</td>"
								res +="</tr>"
							}
						res +="</table>"

						$('#status_dokumen'+number).select2({
					   		width:'100%',
					   	});

						$("#result-file").html(res);
						$("#nama_paket").val(output.paket.nama_paket);
						$("#id_sektor").val(output.paket.id_sektor).trigger('change');
						$("#id_jenis_kegiatan").val(output.paket.id_jenis_kegiatan).trigger('change');
					}else{
						$("#result-file").html("<h3 align='center'>Tidak ada data</h3>");
					}
				}
			});
		}
	}
</script>
@endsection