@extends('app')
@section('title', '-'.empty($paket->id) ? 'Tambah':'Edit'.' Paket')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('paket') }}">Paket</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-paket') }}">@php echo empty($paket->id) ? 'Tambah':'Edit' @endphp Paket</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($paket->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($paket->id) ? 'Tambah':'Edit' @endphp Paket</span>
		</div>
		<div class="actions">
			<a href="{{ route('paket') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-paket" action="{{ route('create-paket') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Kabupaten/Kota <span class="text-red">*</span></label>
							<select class="form-control select2me" data-placeholder="pilih kabupaten" name="id_kabupaten" id="id_kabupaten">
								<option value=""></option>
								@if(count($kabupaten) > 0)
									@foreach($kabupaten as $kabupaten)
									@php
										$selectedKabupaten = "";
										if(!empty($paket->id_kabupaten) && $paket->id_kabupaten == $kabupaten->id){
											$selectedKabupaten = "selected";
										}else if(old('id_kabupaten') == $kabupaten->id){
											$selectedKabupaten = "selected";
										}
									@endphp
									<option value="{{ $kabupaten->id }}" {{ $selectedKabupaten }}>{{ $kabupaten->nama_kabupaten }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<input type="hidden" name="id" id="id" value="{{ !empty($paket->id) ? $paket->id : '' }}">
							<label>Sektor <span class="text-red">*</span></label>
							<select class="form-control select2me" data-placeholder="pilih sektor" name="id_sektor" id="id_sektor" onchange="getDetailDocument()">
								<option value=""></option>
								@if(count($sektor) > 0)
									@foreach($sektor as $sektor)
									@php
										$selectedSektor = "";
										if(!empty($paket->id_sektor) && $paket->id_sektor == $sektor->id){
											$selectedSektor = "selected";
										}else if(old('id_sektor') == $sektor->id){
											$selectedSektor = "selected";
										}
									@endphp
									<option value="{{ $sektor->id }}" {{ $selectedSektor }}>{{ $sektor->nama_sektor }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Jenis Kegiatan <span class="text-red">*</span></label>
							<select class="form-control select2me" data-placeholder="pilih jenis kegiatan" name="id_jenis_kegiatan" id="id_jenis_kegiatan">
								<option value=""></option>
								@if(count($kegiatan) > 0)
									@foreach($kegiatan as $kegiatan)
									@php
										$selectedKegiatan = "";
										if(!empty($paket->id_jenis_kegiatan) && $paket->id_jenis_kegiatan == $kegiatan->id){
											$selectedKegiatan = "selected";
										}else if(old('id_jenis_kegiatan') == $kegiatan->id){
											$selectedKegiatan = "selected";
										}
									@endphp
									<option value="{{ $kegiatan->id }}" {{ $selectedKegiatan }}>{{ $kegiatan->nama_kegiatan }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama Paket <span class="text-red">*</span></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="nama_paket" id="nama_paket" value="{{ !empty($paket->nama_paket) ? $paket->nama_paket : old('nama_paket') }}" class="form-control" placeholder="nama paket">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Status <span class="text-red">*</span></label>
							<select class="form-control select2me" name="status" id="status">
								<option value="">pilih status</option>
								@php
									$selectedStatusActive = "";
									if(isset($paket->status) && $paket->status == "1"){
										$selectedStatusActive = "selected";
									}else if(old('status') == "1"){
										$selectedStatusActive = "selected";
									}

									$selectedStatusNonActive = "";
									if(isset($paket->status) && $paket->status == "0"){
										$selectedStatusNonActive = "selected";
									}else if(old('status') == "0"){
										$selectedStatusNonActive = "selected";
									}
								@endphp
								<option value="1" {{ $selectedStatusActive }}>Aktif</option>
								<option value="0" {{ $selectedStatusNonActive }}>Non Aktif</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Keterangan</label>
							<textarea name="keterangan" id="keterangan" rows="2" class="form-control" placeholder="keterangan">{{ !empty($paket->keterangan) ? $paket->keterangan : old('keterangan') }}</textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<a href="#" id="btn-add" style="margin-bottom:10px;" class="btn btn-circle btn-sm blue"><i class="fa fa-plus"></i> Tambah Document</a>
					<table class="table table-bordered append-file">
						<tr style="font-family:calibri;">
							<th>Nama Dokumen</th>
							<th>Tampilkan Upload File</th>
							<th>Aksi</th>
						</tr>
						@if(!empty($paket->RefDetailPaket))
							@foreach($paket->RefDetailPaket as $detailFile)
							<tr class="user_data">
								<td width="350">
									<input required type="text" name="nama_kegiatan[]" id="nama_kegiatan" class="form-control" value="{{ $detailFile->nama_kegiatan }}" placeholder="Nama Kegiatan">
								</td>
								<td width="100">
									<select required class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload">
										<option value=""></option>
										<option value="1" {{ $detailFile->view_upload == 1 ? "selected": "" }}>YA</option>
										<option value="0" {{ $detailFile->view_upload == 0 ? "selected": "" }}>TIDAK</option>
									</select>
								</td>
								<td width="50">
									<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>
								</td>
							</tr>
							@endforeach
						@else
						<tr class="user_data">
							<td width="350">
								<input type="text" name="nama_kegiatan[]" id="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan">
							</td>
							<td width="100">
								<select class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload">
									<option value="1">YA</option>
									<option value="0">TIDAK</option>
								</select>
							</td>
							<td width="50">
								<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>
							</td>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	var count = 2; 
	$("#btn-add").click(function(e){
		count++;
		e.preventDefault();
		$(".append-file").append(
			'<tr class="user_data">'
				+'<td width="350">'
					+'<input required type="text" name="nama_kegiatan[]" id="nama_kegiatan'+count+'" class="form-control" placeholder="Nama Kegiatan">'
				+'</td>'
				+'<td width="100">'
					+'<select required class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload'+count+'">'
						+'<option value="1">YA</option>'
						+'<option value="0">TIDAK</option>'
					+'</select>'
				+'</td>'
				+'<td width="50">'
					+'<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>'
				+'</td>'
			+'</tr>'
	    );

		$('#view_upload'+count).select2({
	   		width:'100%',
	   	});
	});

	$('.append-file').on('click', '.remove_item', function(e) {
	    e.preventDefault();

	    $(this).parents('.user_data').remove();
	});

	function sumData () {
		var number = [];
		$("input[name='nama_kegiatan[]']").each(function() {
			number.push($(this).val());
		});

		return number.length;
	}

	function validateData () {
		var result = true;
		$("input[name='nama_kegiatan[]']").each(function() {
			var nama_kegiatan = $(this).val();
			if (nama_kegiatan == "") {
				result = false;
			}
		});

		return result;
	}

	$("#btn-submit-act").click(function(){
		if ($("#id_jenis_kegiatan").val() == "") {
			alert("jenis kegiatan belum dipilih");
		}else if ($("#id_sektor").val() == "") {
			alert("sektor belum dipilih");
		}else if ($("#status").val() == "") {
			alert("status belum dipilih");
		}else if (sumData() == 0) {
			alert("belum ada data yang diisi");
		}else if(validateData() == false){
			alert("silahkan lengkapi data");
		}else{
			$("#form-submit-paket").submit();
		}
		
	});
</script>
@endsection