@extends('app')
@section('title', 'Nilai Perencanaan')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('perencanaan') }}">Nilai Perencanaan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('nilai-perencanaan', $perencanaan->id) }}">Nilai Perencanaan</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($perencanaan->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Nilai Perencanaan</span>
		</div>
		<div class="actions">
			<a href="{{ route('perencanaan') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-perencanaan" action="{{ route('update-penilaian-perencanaan') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="id" id="id" value="{{ !empty($perencanaan->id) ? $perencanaan->id : '' }}">
			<div class="form-body">
				<h3><b>{{ !empty($perencanaan->RefKabupaten->nama_kabupaten) ? $perencanaan->RefKabupaten->nama_kabupaten : '' }}</b></h3> <br>
				<table class="table" style="font-family:calibri;">
					<tr>
						<th width="100">Nama Paket</th>
						<td>: {{ !empty($perencanaan->nama_paket) ? $perencanaan->nama_paket : '' }}</td>

						<th width="100">Volume</th>
						<td>: {{ !empty($perencanaan->volume) ? $perencanaan->volume : '' }}</td>
					</tr>
					<tr>
						<th width="100">Sektor</th>
						<td>: {{ !empty($perencanaan->RefSektor->nama_sektor) ? $perencanaan->RefSektor->nama_sektor : '' }}</td>

						<th width="100">Satuan</th>
						<td>: {{ !empty($perencanaan->satuan) ? $perencanaan->satuan : '' }}</td>
					</tr>
					<tr>
						<th width="100">Jenis Kegiatan</th>
						<td>: {{ !empty($perencanaan->RefJenisKegiatan->nama_kegiatan) ? $perencanaan->RefJenisKegiatan->nama_kegiatan : '' }}</td>

						<th width="100">Nilai APBN</th>
						<td>: {{ !empty($perencanaan->nilai_apbn) ? number_format($perencanaan->nilai_apbn) : '' }}</td>
					</tr>
					<tr>
						<th width="100">Kategori</th>
						<td>: {{ !empty($perencanaan->RefKategori->nama_kategori) ? $perencanaan->RefKategori->nama_kategori : '' }}</td>

						<th width="100">Periode</th>
						<td>: {{ !empty($perencanaan->periode) ? $perencanaan->periode : '' }}</td>
					</tr>
					<tr>
						<th width="100">Lokasi</th>
						<td>: {{ !empty($perencanaan->lokasi) ? $perencanaan->lokasi : '' }}</td>

						<th width="100">Keterangan</th>
						<td>: {{ !empty($perencanaan->keterangan) ? $perencanaan->keterangan : '' }}</td>
					</tr>
				</table>
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab1" data-toggle="tab"><i class="fa fa-file"></i> Upload Dokumen</a>
						</li>
					</ul>
					<div class="tab-content no-space">
						<div class="tab-pane active" id="tab1"><br>
							<h4 align="center">Readiness Criteria</h4> <br>
							<div id="result-file"></div>
							@if(!empty($perencanaan->RefDetailKegiatan))
								<table class="table table-hover table-bordered">
									<tr style="font-family:calibri;">
										<th width="50">No</th>
										<th>Nama Dokumen</th>
										<th width="100">Status</th>
										<th width="100">File</th>
										<th width="80">View File</th>
										<th width="200">Status Penilaian</th>
										<th width="100">Score</th>
									</tr>
									@php
										$sumScore = 0;
									@endphp
									@foreach($perencanaan->RefDetailKegiatan as $keys => $detail)
										@php
											$sumScore += $detail->score_dokumen;
										@endphp
									<tr style="font-family:calibri;">
										<td>{{ $loop->iteration }}</td>
										<td>
											{{ $detail->nama_kegiatan }}
											<input type="hidden" name="id_detail[]" id="id_detail[]" value="{{ $detail->id }}">
											<input type="hidden" name="nama_kegiatan[]" id="nama_kegiatan[]" value="{{ $detail->nama_kegiatan }}">
										</td>
										<td>
											@php
												$statusDoc = "";
												if(isset($detail->status_dokumen) && $detail->status_dokumen == "1"){
													$statusDoc = "YA";
												}elseif(isset($detail->status_dokumen) && $detail->status_dokumen == "0"){
													$statusDoc = "TIDAK";
												}

												echo $statusDoc;
											@endphp
											<input type="hidden" name="status_dokumen[]" id="status_dokumen[]" value="{{ $detail->status_dokumen }}">
										</td>
										<td>
											@php
												$gets = App\Models\DetailPaket::where('id', $detail->id_detail_paket)->first();
											@endphp

											@if($detail->file_dokumen != null)
											<a href="{{url('uploads/file/'.$perencanaan->id.'/'.$detail->file_dokumen)}}" download>{{ $detail->file_dokumen }} <i class="fa fa-download"></i></a>
											@elseif((!empty($gets->view_upload) && $gets->view_upload == 1) && $detail->file_dokumen == null)
											-
											@endif
										</td>
										<td>
											@if($detail->file_dokumen != null)
											<a href="#modal-view-file" data-toggle="modal" data-name="{{ $detail->nama_kegiatan }}" data-url="{{url('uploads/file/'.$perencanaan->id.'/'.$detail->file_dokumen)}}" onclick="getFile(this)" class="btn btn-sm blue"><i class="fa fa-eye"></i></a>
											@endif
										</td>
										<td>
											@if($detail->file_dokumen == null && (!empty($gets->view_upload) && $gets->view_upload == 1))
											<i>Belum upload file</i>
											<input type="hidden" name="penilaian_per_doc[]" id="penilaian_per_doc{{ $keys }}" value="3">
											<input type="hidden" name="catatan[]" id="catatan{{ $keys }}">
											@else
											<select class="form-control select2me" name="penilaian_per_doc[]" id="penilaian_per_doc{{ $keys }}" data-key="{{ $keys }}" onchange="selectPenilaianPerDoc(this)">
												@php
													$selectedOK = "";
													if(isset($detail->penilaian_per_doc) && $detail->penilaian_per_doc == "1"){
														$selectedOK = "selected";
													}

													$selectedRevisi = "";
													if(isset($detail->penilaian_per_doc) && $detail->penilaian_per_doc == "2"){
														$selectedRevisi = "selected";
													}
												@endphp
												<option value="1" {{ $selectedOK }}>OK</option>
												<option value="2" {{ $selectedRevisi }}>REVISI</option>
											</select> <br><br>
											@if(isset($detail->penilaian_per_doc) && $detail->penilaian_per_doc == "2")
											<textarea rows="2" class="form-control catatans" name="catatan[]" id="catatan{{ $keys }}" placeholder="catatan">{{ $detail->catatan }}</textarea>
											@else
											<textarea rows="2" class="form-control catatan" name="catatan[]" id="catatan{{ $keys }}" placeholder="catatan">{{ $detail->catatan }}</textarea>
											@endif

											@endif
										</td>
										<td>
											@php
												$readonlyScore = "";
												if($detail->file_dokumen == null && (!empty($gets->view_upload) && $gets->view_upload == 1)){
													$readonlyScore = "readonly";
												}elseif($detail->penilaian_per_doc == 2){
													$readonlyScore = "readonly";
												}
											@endphp
											<input type="text" name="score_dokumen[]" id="score_dokumen{{ $keys }}" class="form-control" {{ $readonlyScore }} value="{{ $detail->score_dokumen }}" onkeyup="sum_score()">
										</td>
									</tr>
									@endforeach
									<tr>
										<th colspan="6">Total Score</th>
										<th>
											<input type="text" class="form-control" id="totalScore" readonly value="{{ $sumScore }}">
										</th>
									</tr>
								</table>
							@endif
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="modal-view-file" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Lihat File <span id="title-header"></span></h4>
			</div>
			<div class="modal-body">
				<iframe id="view_file" width="100%;" height="500px;"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-circle default">Close</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#id_sektor").val() == "") {
			alert("sektor belum dipilih");
		}else if($("#id_jenis_kegiatan").val() == ""){
			alert("jenis kegiatan belum dipilih");
		}else if($("#id_kategori").val() == ""){
			alert("kategori belum dipilih");
		}else if($("#nama_paket").val() == ""){
			alert("nama paket tidak boleh kosong");
		}else if($("#status").val() == ""){
			alert("status belum dipilih");
		}else{
			$("#form-submit-perencanaan").submit();
		}
		
	});

	function sum_score () {
		var total_score_dokumen = 0;
		$("input[name='score_dokumen[]']").each(function() {
		   var score_dokumen = $(this).val().replace(/,/g, '');
		   if (score_dokumen == 0) {
		   		score_dokumen = 0;
		   }

		   total_score_dokumen += parseInt(score_dokumen);
		});

		$("#totalScore").val(total_score_dokumen);
	}

	$(".catatan").hide();
	function selectPenilaianPerDoc (element) {
		var key = $(element).data('key');
		var penilaian_per_doc = $("#penilaian_per_doc"+key).val();

		if (penilaian_per_doc == 2) {
			$("#catatan"+key).show();
			$("#score_dokumen"+key).val(0);
			$("#score_dokumen"+key).prop('readonly', true);

			sum_score();
		}else{
			$("#catatan"+key).hide();
			$("#score_dokumen"+key).removeAttr('readonly');
		}
	}

	function getFile (element) {
		var url = $(element).data('url');
		var name = $(element).data('name');
		$("#view_file").attr("src", url);
		$("#title-header").text(name);
	}
</script>
@endsection