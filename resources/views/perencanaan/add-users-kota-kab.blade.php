@extends('app')
@section('title', '-'.empty($users->id) ? 'Tambah':'Edit'.' Users')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('users') }}">Users</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-users') }}">@php echo empty($users->id) ? 'Tambah':'Edit' @endphp Users</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($users->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($users->id) ? 'Tambah':'Edit' @endphp Users</span>
		</div>
		<div class="actions">
			<a href="{{ route('users') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-users" action="{{ route('create-users') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<input type="hidden" name="id" id="id" value="{{ !empty($users->id_login) ? $users->id_login : '' }}">
							<label>Kota/Kab <span class="text-red">*</span></label>
							<select class="form-control select2me" data-placeholder="pilih kota/kab" name="id_kabupaten" id="id_kabupaten">
								<option value=""></option>
								@if(count($kabupaten) > 0)
									@foreach($kabupaten as $kab)
									@php
										$gets = App\Models\UsersKotaKab::where('id_kabupaten', $kab->id)->get();
									@endphp
									@if(count($gets) == 0)
									@php
										$selectedKab = "";
										if(!empty($users->id_kabupaten) && $users->id_kabupaten == $kab->id){
											$selectedKab = "selected";
										}else if(old('id_kabupaten') == $kab->id){
											$selectedKab = "selected";
										}
									@endphp
									<option value="{{ $kab->id }}" {{ $selectedKab }}>{{ $kab->nama_kabupaten }}</option>
									@endif
									@endforeach
								@endif
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Username <span class="text-red">*</span></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="username" id="username" value="{{ !empty($users->RefLogin->username) ? $users->RefLogin->username : old('username') }}" class="form-control" placeholder="username">
							</div>
						</div>
					</div>
					@if(empty($users->id))
					<div class="col-md-4">
						<div class="form-group">
							<label>Password <span class="text-red">*</span></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="password" id="password" value="{{ !empty($users->RefLogin->password) ? $users->RefLogin->password : old('password') }}" class="form-control" placeholder="password">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Email</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="email" id="email" value="{{ !empty($users->email) ? $users->email : old('email') }}" class="form-control" placeholder="email">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>No Telp</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="no_telp" id="no_telp" value="{{ !empty($users->no_telp) ? $users->no_telp : old('no_telp') }}" class="form-control" placeholder="no telp">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Status <span class="text-red">*</span></label>
							<select class="form-control select2me" name="status" id="status">
								<option value="">pilih status</option>
								@php
									$selectedStatusActive = "";
									if(isset($users->RefLogin->status) && $users->RefLogin->status == "1"){
										$selectedStatusActive = "selected";
									}else if(old('status') == "1"){
										$selectedStatusActive = "selected";
									}

									$selectedStatusNonActive = "";
									if(isset($users->RefLogin->status) && $users->RefLogin->status == "0"){
										$selectedStatusNonActive = "selected";
									}else if(old('status') == "0"){
										$selectedStatusNonActive = "selected";
									}
								@endphp
								<option value="1" {{ $selectedStatusActive }}>Aktif</option>
								<option value="0" {{ $selectedStatusNonActive }}>Non Aktif</option>
							</select>
						</div>
					</div>
					
					@endif
					<div class="col-md-6">
						<div class="form-group">
							<label>Alamat</label>
							<textarea name="alamat" id="alamat" rows="4" class="form-control" placeholder="alamat">{{ !empty($users->alamat) ? $users->alamat : old('alamat') }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#id_kabupaten").val() == "") {
			alert("kota/kab belum dipilih");
		}else if($("#username").val() == ""){
			alert("username belum diisi");
		}else if($("#password").val() == ""){
			alert("password belum diisi");
		}else if($("#status").val() == ""){
			alert("status belum dipilih");
		}else{
			$("#form-submit-users").submit();
		}
		
	});
</script>
@endsection