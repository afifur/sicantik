@extends('app')
@section('title', '- Perencanaan')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('perencanaan') }}">Perencanaan</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Perencanaan</span>
			<span class="caption-helper">daftar perencanaan</span>
		</div>
		<div class="actions">
			@if(session()->get('type') == 1)
			<a href="{{ route('add-perencanaan') }}" class="btn btn-circle btn-primary btn-sm">
				<i class="fa fa-plus"></i> Tambah Baru 
			</a> &nbsp;
			@endif
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif
		<div class="table-toolbar">
			@if(session()->get('type') == 2)
			<form id="submit-form" action="{{ route('perencanaan') }}" method="GET">
			    <div class="row">
			    	<div class="col-md-3">
			    		<div class="form-group">
							<label>Sektor</label>
							<select class="form-control select2me" data-placeholder="pilih sektor" name="id_sektor" id="id_sektor">
								<option value=""></option>
								@if(count($sektor) > 0)
									@foreach($sektor as $sektor)
									@php
										$selectedSektor = "";
										if(!empty($_GET['id_sektor']) && $_GET['id_sektor'] == $sektor->id){
											$selectedSektor = "selected";
										}
									@endphp
									<option value="{{ $sektor->id }}" {{ $selectedSektor }}>{{ $sektor->nama_sektor }}</option>
									@endforeach
								@endif
							</select>
						</div>
			    	</div>
			    	<div class="col-md-3">
			    		<div class="form-group">
							<label>Kabupaten</label>
							<select class="form-control select2me" data-placeholder="Pilih Kabupaten" name="id_kabupaten" id="id_kabupaten">
								<option value=""></option>
								@if(count($kabupaten) > 0)
									@foreach($kabupaten as $kabupaten)
									@php
										$selectedKabupaten = "";
										if(!empty($_GET['id_kabupaten']) && $_GET['id_kabupaten'] == $kabupaten->id){
											$selectedKabupaten = "selected";
										}
									@endphp
									<option value="{{ $kabupaten->id }}" {{ $selectedKabupaten }}>{{ $kabupaten->nama_kabupaten }}</option>
									@endforeach
								@endif
							</select>
						</div>
			    	</div>
			    	<div class="col-md-3">
			    		<div class="form-group">
							<label>Periode</label>
							<select class="form-control select2me" data-placeholder="Pilih Periode" name="periode" id="periode" onchange="filterPeriode()">
								<option value=""></option>
								@for($i=date('Y'); $i <= date('Y') + 10; $i++)
								@php
									$selectedYear = "";
									if(isset($_GET['periode']) && $_GET['periode'] == $i){
										$selectedYear = "selected";
									}
								@endphp
								<option value="{{ $i }}" {{ $selectedYear }}>{{ $i }}</option>
								@endfor
							</select>
						</div>
			    	</div>
			    	<div class="col-md-1">
			    		<label>&nbsp;</label><br>
			    		<button class="btn blue" type="submit" name="filter" id="filter" value="submit">Cari</button>
			    	</div>
			    </div>
			</form>
			@endif
			<table class="table table-striped table-bordered table-hover datatables" id="perencanaan">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Paket</th>
						<th>Jenis Kegiatan</th>
						<th>Lokasi</th>
						<th>Volume</th>
						<th>Satuan</th>
						<th>Nilai APBN</th>
						<th>Kategori</th>
						<th>Score</th>
						<th>Status Verifikasi</th>
						<th width="80" style="text-align:right;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
					<tr style="font-family:calibri;">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $values->nama_paket }}</td>
						<td>{{ $values->RefJenisKegiatan->nama_kegiatan }}</td>
						<td>{{ $values->lokasi }}</td>
						<td>{{ $values->volume }}</td>
						<td>{{ $values->satuan }}</td>
						<td>{{ number_format($values->nilai_apbn) }}</td>
						<td>{{ $values->RefKategori->nama_kategori }}</td>
						<td>
							@php
								$score = array();
								foreach($values->RefDetailKegiatan as $kegiatan){
									$score[] = $kegiatan->score_dokumen;
								}

								echo array_sum($score);
							@endphp
						</td>
						<td>
							@if($values->status_penilaian == 1)
							<label class="label label-success">Sudah Verifikasi</label>
							@else
							<label class="label label-danger">Belum Verifikasi</label>
							@endif
						</td>
						<td align="right">
							@if(session()->get('type') == 2)
							<a href="{{ route('nilai-perencanaan', $values->id) }}" class="btn btn-sm yellow tooltips" data-placement="left" data-original-title="Penilaian"><i class="fa fa-slack"></i></a>
							@else
							<a href="{{ route('edit-perencanaan', $values->id) }}" class="btn btn-sm blue tooltips" data-placement="left" data-original-title="Edit perencanaan"><i class="fa fa-edit"></i></a>
							@endif
							<a href="{{ route('delete-perencanaan', $values->id) }}" onClick="return confirm('ingin menghapus data ini ?');" class="btn btn-sm red tooltips" data-placement="left" data-original-title="Hapus perencanaan"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection