@extends('app')
@section('title', '- Paket')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('paket') }}">Paket</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Paket</span>
			<span class="caption-helper">daftar paket</span>
		</div>
		<div class="actions">
			<a href="{{ route('add-paket') }}" class="btn btn-circle btn-primary btn-sm">
				<i class="fa fa-plus"></i> Tambah Baru 
			</a> &nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif
		<div class="table-toolbar">
			<table class="table table-striped table-bordered table-hover datatables" id="paket">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Paket</th>
						<th>Sektor</th>
						<th>Jenis Kegiatan</th>
						<th>Kabupaten/Kota</th>
						<th>Keterangan</th>
						<th width="80" style="text-align:right;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
					<tr style="font-family:calibri;">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $values->nama_paket }}</td>
						<td>{{ $values->RefSektor->nama_sektor }}</td>
						<td>{{ $values->RefJenisKegiatan->nama_kegiatan }}</td>
						<td>{{ !empty($values->RefKabupaten->nama_kabupaten) ? $values->RefKabupaten->nama_kabupaten : '' }}</td>
						<td>{{ $values->keterangan }}</td>
						<td align="right">
							<a href="{{ route('edit-paket', $values->id) }}" class="btn btn-sm blue tooltips" data-placement="left" data-original-title="Edit paket"><i class="fa fa-edit"></i></a>
							<a href="{{ route('delete-paket', $values->id) }}" onClick="return confirm('ingin menghapus data ini ?');" class="btn btn-sm red tooltips" data-placement="left" data-original-title="Hapus paket"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection