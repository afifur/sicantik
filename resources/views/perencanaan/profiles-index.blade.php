@extends('app')
@section('title', '- Profile')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('profiles') }}">Profile</a>
		</li>
	</ul>
</div> <br>
@if(session()->get('type') == 2)
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Upload Slide</span>
			<span class="caption-helper">upload slide dashboard</span>
		</div>
		<div class="actions">
			<button type="button" id="btn-submit-act2" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success_slide = Session::get('success_slide'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success_slide }}</strong>
			</div>
		@endif

		<form role="form" id="form-submit-slide" action="{{ route('create-slide') }}" method="POST" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label>Upload Foto</label>
				<input type="file" required name="file_content" id="file_content">
			</div>
		</form> <br>
		<h4>Daftar Slide</h4>
		<table class="table table-bordered append-file">
			<tr style="font-family:calibri;">
				<th>Foto Slide</th>
				<th>Aksi</th>
			</tr>
			@if(count($contents) > 0)
				@foreach($contents as $contents)
				<tr class="user_data">
					<td><img src="{{url('uploads/slide/'.$contents->file_content)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /></td>
					<td><a href="{{ route('delete-slide', $contents->id) }}" onclick="return confirm('ingin menghapus data ini ?')" class="btn red"><i class="fa fa-remove"></i></a></td>
				</tr>
				@endforeach
			
			@endif
		</table>
	</div>
</div>
@endif
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Profile</span>
			<span class="caption-helper">daftar profile</span>
		</div>
		<div class="actions">
			@if(session()->get('type') == 2)
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			@endif
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if(session()->get('type') == 1)
			<h3>{{ !empty($profiles->title) ? $profiles->title : '' }}</h3>
			<p>{{ !empty($profiles->deskripsi) ? $profiles->deskripsi : '' }}</p>
			<br>
			<h3>Struktur Organisasi</h3>
			<table style="font-family:calibri;" class="table table-bordered table-hover" id="profile">
				<thead>
					<tr>
						<th>No</th>
						<th width="100">Foto</th>
						<th>Nama</th>
						<th>Jabatan</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>
							@if($profiles->foto_kepala_balai1 != null)
							<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kepala_balai1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
							@else
							<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
							@endif
						</td>
						<td>
							{{ !empty($profiles->nama_kepala_balai1) ? $profiles->nama_kepala_balai1 : '' }}
						</td>
						<td>
							Kepala Balai PPW Maluku
						</td>
					</tr>
					<tr>
						<td>2</td>
						<td>
							@if($profiles->foto_kasubag != null)
							<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasubag)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
							@else
							<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
							@endif
						</td>
						<td>
							{{ !empty($profiles->nama_kasubag) ? $profiles->nama_kasubag : '' }}
						</td>
						<td>
							Kasubag Tata Usaha
						</td>
					</tr>
					<tr>
						<td>3</td>
						<td>
							@if($profiles->foto_kasie_wil1 != null)
							<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
							@else
							<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
							@endif
						</td>
						<td>
							{{ !empty($profiles->nama_kasie_wil1) ? $profiles->nama_kasie_wil1 : '' }}
						</td>
						<td>
							Kasie Pelaksana Wilayah 1
						</td>
					</tr>
					<tr>
						<td>4</td>
						<td>
							@if($profiles->foto_kasie_wil2 != null)
							<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil2)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
							@else
							<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
							@endif
						</td>
						<td>
							{{ !empty($profiles->nama_kasie_wil2) ? $profiles->nama_kasie_wil2 : '' }}
						</td>
						<td>
							Kasie Pelaksana Wilayah 2
						</td>
					</tr>
					<tr>
						<td>5</td>
						<td>
							@if($profiles->foto_kasatker != null)
							<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasatker)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
							@else
							<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
							@endif
						</td>
						<td>
							{{ !empty($profiles->nama_kasatker) ? $profiles->nama_kasatker : '' }}
						</td>
						<td>
							Kasataker Pelaksanaan PPW Maluku
						</td>
					</tr>
				</tbody>
			</table>
		@else
			<form role="form" id="form-submit-profiles" action="{{ route('create-profiles') }}" method="POST" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="id" id="id" value="{{ !empty($profiles->id) ? $profiles->id : '' }}">
				<div class="table-toolbar">
					<div class="form-group">
						<label>Title</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
							<input type="text" name="title" id="title" value="{{ !empty($profiles->title) ? $profiles->title : old('title') }}" class="form-control" placeholder="Title">
						</div>
					</div>
					<div class="form-group">
						<label>Deskripsi</label>
						<textarea name="deskripsi" id="deskripsi" class="form-control" rows="5" placeholder="Deskripsi">{{ !empty($profiles->deskripsi) ? $profiles->deskripsi : old('deskripsi') }}</textarea>
					</div> <br>
					<h4>Struktur Organisasi</h4>
					<table style="font-family:calibri;" class="table table-striped table-bordered table-hover" id="profile">
						<thead>
							<tr>
								<th>No</th>
								<th width="100">Foto</th>
								<th>Nama</th>
								<th>Jabatan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									@if($profiles->foto_kepala_balai1 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kepala_balai1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@endif
									<input type="file" name="foto_kepala_balai1" id="foto_kepala_balai1">
								</td>
								<td>
									<input type="text" class="form-control" name="nama_kepala_balai1" id="nama_kepala_balai1" value="{{ !empty($profiles->nama_kepala_balai1) ? $profiles->nama_kepala_balai1 : old('nama_kepala_balai1') }}" placeholder="nama" />
								</td>
								<td>
									<input type="text" class="form-control" name="jabatan_kepala_balai1" id="jabatan_kepala_balai1" value="Kepala Balai PPW Maluku" readonly>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>
									@if($profiles->foto_kasubag != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasubag)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@endif
									<input type="file" name="foto_kasubag" id="foto_kasubag">
								</td>
								<td>
									<input type="text" class="form-control" name="nama_kasubag" id="nama_kasubag" value="{{ !empty($profiles->nama_kasubag) ? $profiles->nama_kasubag : old('nama_kasubag') }}" placeholder="nama" />
								</td>
								<td>
									<input type="text" class="form-control" name="jabatan_kasubag" id="jabatan_kasubag" value="Kasubag Tata Usaha" readonly>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>
									@if($profiles->foto_kasie_wil1 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@endif
									<input type="file" name="foto_kasie_wil1" id="foto_kasie_wil1">
								</td>
								<td>
									<input type="text" class="form-control" name="nama_kasie_wil1" id="nama_kasie_wil1" value="{{ !empty($profiles->nama_kasie_wil1) ? $profiles->nama_kasie_wil1 : old('nama_kasie_wil1') }}" placeholder="nama" />
								</td>
								<td>
									<input type="text" class="form-control" name="jabatan_kasie_wil1" id="jabatan_kasie_wil1" value="Kasie Pelaksana Wilayah 1" readonly>
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>
									@if($profiles->foto_kasie_wil2 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil2)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@endif
									<input type="file" name="foto_kasie_wil2" id="foto_kasie_wil2">
								</td>
								<td>
									<input type="text" class="form-control" name="nama_kasie_wil2" id="nama_kasie_wil2" value="{{ !empty($profiles->nama_kasie_wil2) ? $profiles->nama_kasie_wil2 : old('nama_kasie_wil2') }}" placeholder="nama" />
								</td>
								<td>
									<input type="text" class="form-control" name="jabatan_kasie_wil2" id="jabatan_kasie_wil2" value="Kasie Pelaksana Wilayah 2" readonly>
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>
									@if($profiles->foto_kasatker != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasatker)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@endif
									<input type="file" name="foto_kasatker" id="foto_kasatker">
								</td>
								<td>
									<input type="text" class="form-control" name="nama_kasatker" id="nama_kasatker" value="{{ !empty($profiles->nama_kasatker) ? $profiles->nama_kasatker : old('nama_kasatker') }}" placeholder="nama" />
								</td>
								<td>
									<input type="text" class="form-control" name="jabatan_kasatker" id="jabatan_kasatker" value="Kasataker Pelaksanaan PPW Maluku" readonly>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
		@endif
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#title").val() == "") {
			alert("title harus diisi");
		}else{
			$("#form-submit-profiles").submit();
		}
		
	});

	$("#btn-submit-act2").click(function(){
		$("#form-submit-slide").submit();
	});
	
</script>
@endsection