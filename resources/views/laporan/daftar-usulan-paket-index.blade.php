@extends('app')
@section('title', '- Laporan Daftar Usulan Paket')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('perencanaan') }}">Laporan Daftar Usulan Paket</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Laporan Daftar Usulan Paket</span>
		</div>
		<div class="actions">
			@if(session()->get('type') == 2)
			<div class="btn-group">
				<a class="btn btn-circle btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
				<i class="fa fa-download"></i> Export <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="{{ route('export-pdf-usulan-paket', !empty($_GET) ? $_GET : '' ) }}"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
					</li>
					<li>
						<a href="{{ route('export-excel-usulan-paket', !empty($_GET) ? $_GET : '' ) }}"><i class="fa fa-file-excel-o"></i> Export Excel</a>
					</li>
				</ul>
			</div> &nbsp;
			@endif
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif
		<div class="table-toolbar">
			<form id="submit-form" action="{{ route('perencanaan') }}" method="GET">
			    <div class="row">
			    	<div class="col-md-3">
			    		<div class="form-group">
							<label>Sektor</label>
							<select class="form-control select2me" data-placeholder="pilih sektor" name="id_sektor" id="id_sektor" onchange="filterSektor()">
								<option value=""></option>
								@if(count($sektor) > 0)
									@foreach($sektor as $sektor)
									@php
										$selectedSektor = "";
										if(!empty($_GET['id_sektor']) && $_GET['id_sektor'] == $sektor->id){
											$selectedSektor = "selected";
										}
									@endphp
									<option value="{{ $sektor->id }}" {{ $selectedSektor }}>{{ $sektor->nama_sektor }}</option>
									@endforeach
								@endif
							</select>
						</div>
			    	</div>
			    	@if(session()->get('type') == 2)
			    	<div class="col-md-3">
			    		<div class="form-group">
							<label>Kabupaten</label>
							<select class="form-control select2me" data-placeholder="Pilih Kabupaten" name="id_kabupaten" id="id_kabupaten" onchange="filterKabupaten()">
								<option value=""></option>
								@if(count($kabupaten) > 0)
									@foreach($kabupaten as $kabupaten)
									@php
										$selectedKabupaten = "";
										if(!empty($_GET['id_kabupaten']) && $_GET['id_kabupaten'] == $kabupaten->id){
											$selectedKabupaten = "selected";
										}
									@endphp
									<option value="{{ $kabupaten->id }}" {{ $selectedKabupaten }}>{{ $kabupaten->nama_kabupaten }}</option>
									@endforeach
								@endif
							</select>
						</div>
			    	</div>
			    	@endif
			    	<div class="col-md-2">
			    		<div class="form-group">
							<label>Periode</label>
							<select class="form-control select2me" data-placeholder="Pilih Periode" name="periode" id="periode" onchange="filterPeriode()">
								<option value=""></option>
								@for($i=date('Y'); $i <= date('Y') + 10; $i++)
								@php
									$selectedYear = "";
									if(isset($_GET['periode']) && $_GET['periode'] == $i){
										$selectedYear = "selected";
									}
								@endphp
								<option value="{{ $i }}" {{ $selectedYear }}>{{ $i }}</option>
								@endfor
							</select>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="form-group">
							<label>Verifikasi</label>
							<select class="form-control select2me" data-placeholder="Pilih Verifikasi" name="status_penilaian" id="status_penilaian" onchange="filterVerifikasi()">
								<option value=""></option>
								<option value="1" {{ isset($_GET['status_penilaian']) && $_GET['status_penilaian'] == 1 ? 'selected': '' }}>YA</option>
								<option value="0" {{ isset($_GET['status_penilaian']) && $_GET['status_penilaian'] == 0 ? 'selected': '' }}>TIDAK</option>
							</select>
						</div>
			    	</div>
			    	<div class="col-md-2">
			    		<div class="form-group">
							<label>Tahapan Kegiatan</label>
							<select class="form-control select2me" data-placeholder="pilih tahapan kegiatan" name="id_tahapan_kegiatan" id="id_tahapan_kegiatan" onchange="filterTahapanKegiatan()">
								<option value=""></option>
								@if(count($tahapan_kegiatan) > 0)
									@foreach($tahapan_kegiatan as $tahapan_kegiatan)
									@php
										$selectedTahapanKegiatan = "";
										if(!empty($_GET['id_tahapan_kegiatan']) && $_GET['id_tahapan_kegiatan'] == $tahapan_kegiatan->id){
											$selectedTahapanKegiatan = "selected";
										}
									@endphp
									<option value="{{ $tahapan_kegiatan->id }}" {{ $selectedTahapanKegiatan }}>{{ $tahapan_kegiatan->nama_tahapan_kegiatan }}</option>
									@endforeach
								@endif
							</select>
						</div>
			    	</div>
			    </div>
			</form>
			<table class="table table-striped table-bordered table-hover" id="perencanaan">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Paket</th>
						<th>Jenis Kegiatan</th>
						<th>Kab/Kota</th>
						<th>Lokasi</th>
						<th>Volume</th>
						<th>Satuan</th>
						<th>Nilai APBN</th>
						<th>Kategori</th>
						<th>Score</th>
						<th>Tahapan Kegiatan</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
					<tr style="font-family:calibri;">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $values->nama_paket }}</td>
						<td>{{ $values->RefJenisKegiatan->nama_kegiatan }}</td>
						<td>{{ $values->RefKabupaten->nama_kabupaten }}</td>
						<td>{{ $values->lokasi }}</td>
						<td>{{ $values->volume }}</td>
						<td>{{ $values->satuan }}</td>
						<td>{{ number_format($values->nilai_apbn) }}</td>
						<td>{{ $values->RefKategori->nama_kategori }}</td>
						<td>
							@php
								$score = array();
								foreach($values->RefDetailKegiatan as $kegiatan){
									$score[] = $kegiatan->score_dokumen;
								}

								echo array_sum($score);
							@endphp
						</td>
						<td>
							{{ $values->RefTahapanKegiatan->nama_tahapan_kegiatan }}
						</td>
						<td>{{ $values->keterangan }}</td>
					</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	function filterSektor() {
		var id_sektor = $("#id_sektor").val();
		insertParam('id_sektor', id_sektor);
	}

	function filterKabupaten() {
		var id_kabupaten = $("#id_kabupaten").val();
		insertParam('id_kabupaten', id_kabupaten);
	}

	function filterPeriode() {
		var periode = $("#periode").val();
		insertParam('periode', periode);
	}

	function filterVerifikasi() {
		var status_penilaian = $("#status_penilaian").val();
		insertParam('status_penilaian', status_penilaian);
	}

	function filterTahapanKegiatan() {
		var id_tahapan_kegiatan = $("#id_tahapan_kegiatan").val();
		insertParam('id_tahapan_kegiatan', id_tahapan_kegiatan);
	}

	function insertParam(key,value)
	{
	    key = encodeURIComponent(key); value = encodeURIComponent(value);

	    var s = document.location.search;
	    var kvp = key+"="+value;

	    var r = new RegExp("(&|\\?)"+key+"=[^\&]*");

	    s = s.replace(r,"$1"+kvp);

	    if(!RegExp.$1) {s += (s.length>0 ? '&' : '?') + kvp;};

	    //again, do what you will here
	    document.location.search = s;
	}
</script>
@endsection