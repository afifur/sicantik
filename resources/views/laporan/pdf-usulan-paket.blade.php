<html>
	<head>
		<title>Laporan Daftar Usulan Paket</title>
	</head>
		<body>
			<div class="header-report">
				<h3 align="center" style="font-family:Roboto;">Laporan Daftar Usulan Paket</h3>
			</div>
			<table border="1" style="border-collapse:collapse; border:solid 1px #999999;" width="100%">
				<thead>
					<tr style="font-family:calibri; font-size:14px;">
						<th>No</th>
						<th>Nama Paket</th>
						<th>Jenis Kegiatan</th>
						<th>Kab/Kota</th>
						<th>Lokasi</th>
						<th>Volume</th>
						<th>Satuan</th>
						<th>Nilai APBN</th>
						<th>Kategori</th>
						<th>Score</th>
						<th>Tahapan Kegiatan</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
						<tr style="font-family:calibri; font-size:11px;">
							<td>{{ $loop->iteration }}</td>
							<td>{{ $values->nama_paket }}</td>
							<td>{{ $values->RefJenisKegiatan->nama_kegiatan }}</td>
							<td>{{ $values->RefKabupaten->nama_kabupaten }}</td>
							<td>{{ $values->lokasi }}</td>
							<td>{{ $values->volume }}</td>
							<td>{{ $values->satuan }}</td>
							<td>{{ number_format($values->nilai_apbn) }}</td>
							<td>{{ $values->RefKategori->nama_kategori }}</td>
							<td>
								@php
									$score = array();
									foreach($values->RefDetailKegiatan as $kegiatan){
										$score[] = $kegiatan->score_dokumen;
									}

									echo array_sum($score);
								@endphp
							</td>
							<td></td>
							<td>{{ $values->keterangan }}</td>
						</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</body>
</html>