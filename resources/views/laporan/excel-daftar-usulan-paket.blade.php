<table style="border:solid 1px #000000;">
	<thead>
		<tr>
			<th colspan="12" style="text-align:center; font-size:20px">
				Laporan Daftar Usulan Paket
			</th>
		</tr>
		<tr><th>&nbsp;</th></tr>
		<tr>
			<th>No</th>
			<th>Nama Paket</th>
			<th>Jenis Kegiatan</th>
			<th>Kab/Kota</th>
			<th>Lokasi</th>
			<th>Volume</th>
			<th>Satuan</th>
			<th>Nilai APBN</th>
			<th>Kategori</th>
			<th>Score</th>
			<th>Tahapan Kegiatan</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data) > 0)
			@foreach($data as $values)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td style="width:20px;">{{ $values->nama_paket }}</td>
				<td style="width:20px;">{{ $values->RefJenisKegiatan->nama_kegiatan }}</td>
				<td style="width:20px;">{{ $values->RefKabupaten->nama_kabupaten }}</td>
				<td style="width:10px;">{{ $values->lokasi }}</td>
				<td style="width:10px;">{{ $values->volume }}</td>
				<td style="width:10px;">{{ $values->satuan }}</td>
				<td style="width:20px;">{{ number_format($values->nilai_apbn) }}</td>
				<td style="width:10px;">{{ $values->RefKategori->nama_kategori }}</td>
				<td style="width:10px;">
					@php
						$score = array();
						foreach($values->RefDetailKegiatan as $kegiatan){
							$score[] = $kegiatan->score_dokumen;
						}

						echo array_sum($score);
					@endphp
				</td>
				<td style="width:10px;"></td>
				<td style="width:10px;">{{ $values->keterangan }}</td>
			</tr>
			@endforeach
		@endif
	</tbody>
</table>