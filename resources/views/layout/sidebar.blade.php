<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="sidebar-toggler-wrapper">
				<div class="sidebar-toggler">
				</div>
			</li> <br>

			@php
				$activeDashboard = '';
				if(isset(request()->segments(1)[0])){
					if(request()->segments(1)[0] == 'dashboard'){
						$activeDashboard = 'start active open';
					}
				}
			@endphp
			<li class="{{ $activeDashboard }}">
				<a href="{{ route('dashboard') }}">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
				</a>
			</li>

			@if(session()->get('type') == 2)
			<li class="heading">
				<h3 class="uppercase">Data Master</h3>
			</li>

			@php
				$activeJenisKegiatan = '';
				$groupJenisKegiatan = array('jenis-kegiatan', 'add-jenis-kegiatan', 'edit-jenis-kegiatan');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupJenisKegiatan)){
						$activeJenisKegiatan = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeJenisKegiatan }}" data-container="body" data-placement="right" data-html="true" data-original-title="Jenis Kegiatan">
				<a href="{{ route('jenis-kegiatan') }}">
					<i class="icon-docs"></i>
					<span class="title">Jenis Kegiatan </span>
				</a>
			</li>

			@php
				$activeKategori = '';
				$groupKategori = array('kategori', 'add-kategori', 'edit-kategori');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupKategori)){
						$activeKategori = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeKategori }}" data-container="body" data-placement="right" data-html="true" data-original-title="Kategori">
				<a href="{{ route('kategori') }}">
					<i class="icon-folder-alt "></i>
					<span class="title">Kategori </span>
				</a>
			</li>

			@php
				$activeSektor = '';
				$groupSektor = array('sektor', 'add-sektor', 'edit-sektor', 'add-setting-document');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupSektor)){
						$activeSektor = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeSektor }}" data-container="body" data-placement="right" data-html="true" data-original-title="Sektor">
				<a href="{{ route('sektor') }}">
					<i class="icon-map"></i>
					<span class="title">Sektor </span>
				</a>
			</li>

			@php
				$activeTahapanKegiatan = '';
				$groupTahapanKegiatan = array('tahapan-kegiatan', 'add-tahapan-kegiatan', 'edit-tahapan-kegiatan');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupTahapanKegiatan)){
						$activeTahapanKegiatan = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeTahapanKegiatan }}" data-container="body" data-placement="right" data-html="true" data-original-title="tahapan kegiatan">
				<a href="{{ route('tahapan-kegiatan') }}">
					<i class="icon-graph"></i>
					<span class="title">Tahapan Kegiatan </span>
				</a>
			</li>

			@php
				$activeKabupaten = '';
				$groupKabupaten = array('kabupaten', 'add-kabupaten', 'edit-kabupaten');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupKabupaten)){
						$activeKabupaten = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeKabupaten }}" data-container="body" data-placement="right" data-html="true" data-original-title="Kabupaten">
				<a href="{{ route('kabupaten') }}">
					<i class="icon-globe-alt"></i>
					<span class="title">Wilayah </span>
				</a>
			</li>
			@endif

			<li class="heading">
				<h3 class="uppercase">Perencanaan</h3>
			</li>
			@if(session()->get('type') == 2)
			@php
				$activePaket = '';
				$groupPaket = array('paket', 'add-paket', 'edit-paket');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupPaket)){
						$activePaket = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activePaket }}" data-container="body" data-placement="right" data-html="true" data-original-title="paket">
				<a href="{{ route('paket') }}">
					<i class="icon-badge"></i>
					<span class="title">Paket </span>
				</a>
			</li>
			@endif
			
			@php
				$activeProfiles = '';
				$groupProfiles = array('profiles', 'add-profiles', 'edit-profiles');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupProfiles)){
						$activeProfiles = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeProfiles }}" data-container="body" data-placement="right" data-html="true" data-original-title="profile">
				<a href="{{ route('profiles') }}">
					<i class="icon-users"></i>
					<span class="title">Profile </span>
				</a>
			</li>

			@if(session()->get('type') == 2)
			@php
				$activeUsers = '';
				$groupUsers = array('users', 'add-users', 'edit-users');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupUsers)){
						$activeUsers = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeUsers }}" data-container="body" data-placement="right" data-html="true" data-original-title="users">
				<a href="{{ route('users') }}">
					<i class="icon-user-follow"></i>
					<span class="title">Users </span>
				</a>
			</li>
			@endif

			@php
				$activePerencanaan = '';
				$groupPerencanaan = array('perencanaan', 'add-perencanaan', 'edit-perencanaan');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupPerencanaan)){
						$activePerencanaan = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activePerencanaan }}" data-container="body" data-placement="right" data-html="true" data-original-title="perencanaan">
				<a href="{{ route('perencanaan') }}">
					<i class="icon-calendar"></i>
					<span class="title">Perencanaan </span>
				</a>
			</li>

			<li class="heading">
				<h3 class="uppercase">Laporan</h3>
			</li>	

			@php
				$activeDaftarUsulanPaket = '';
				$groupDaftarUsulanPaket = array('lap-daftar-usulan-paket');
				if(isset(request()->segments(1)[0])){
					if(in_array( request()->segments(1)[0], $groupDaftarUsulanPaket)){
						$activeDaftarUsulanPaket = 'start active open';
					}
				}
			@endphp
			<li class="tooltips {{ $activeDaftarUsulanPaket }}" data-container="body" data-placement="right" data-html="true" data-original-title="users">
				<a href="{{ route('lap-daftar-usulan-paket') }}">
					<i class="icon-notebook"></i>
					<span class="title">Daftar Usulan Paket </span>
				</a>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>