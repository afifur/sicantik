<div class="page-header navbar navbar-fixed-top">
	<div class="page-header-inner">
		<div class="page-logo">
			<a href="{{route('dashboard')}}">
				<img src="{{url('assets/global/img/logo_sicantik.png')}}" width="234" height="45" style="position:absolute; left:1px;">
			</a>
			<div class="menu-toggler sidebar-toggler hide">
			</div>
		</div>
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="username username-hide-on-mobile">{{ session()->get('username') }} </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
						<li>
							<a href="{{ route('profile') }}">
							<i class="icon-user"></i>Profile </a>
						</li>
						<li>
							<a href="{{ route('change-password') }}">
							<i class="icon-lock"></i> Ganti Password </a>
						</li>
						<li class="divider">
						</li>
						
						<li>
							<a href="#modal-logout" data-toggle="modal">
							<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="modal-logout" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Konfirmasi</h4>
			</div>
			<div class="modal-body">
				<p>
					 Ingin keluar dari sistem ini ?
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
				<a href="{{ route('logout') }}" class="btn green">Logout</a>
			</div>
		</div>
	</div>
</div>