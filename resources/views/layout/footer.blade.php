<div class="page-footer">
	<div class="page-footer-inner">
		 @php echo date('Y') @endphp &copy; Ceklis.id
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>