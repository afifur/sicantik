@extends('app')
@section('title', '- Dashboard')

@section('content')
	@if ($success = Session::get('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
			<strong>{{ $success }}</strong>
		</div>
	@endif
	<h3 class="page-title">
		Dashboard <small>reports & statistics</small>
	</h3>
	<img src="{{url('assets/global/img/dashboard.JPG')}}" width="100%"><br><br>
	@if(session()->get('type') == 2)
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat blue-madison">
				<div class="visual">
					<i class="fa fa-comments"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($totalUsers) }}
					</div>
					<div class="desc" style="font-size:13px;">
						 Total Users
					</div>
				</div>
				<a class="more" href="{{ route('users') }}">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat red-intense">
				<div class="visual">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($perencanaanVerifikasi) }}
					</div>
					<div class="desc" style="font-size:13px;">
						 Total Perencanaan (Sudah Verifikasi)
					</div>
				</div>
				<a class="more" href="#" onclick="redirectPerencanaanVerifikasi()">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat green-haze">
				<div class="visual">
					<i class="fa fa-shopping-cart"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($perencanaanNotVerifikasi) }}
					</div>
					<div class="desc" style="font-size:13px;">
						Total Perencanaan (Belum Verifikasi)
					</div>
				</div>
				<a class="more" href="#" onclick="redirectPerencanaanNotVerifikasi()">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat purple-plum">
				<div class="visual">
					<i class="fa fa-globe"></i>
				</div>
				<div class="details">
					<div class="number" >
						 {{ number_format($totalSektor) }}
					</div>
					<div class="desc" style="font-size:13px;">
						Total Sektor
					</div>
				</div>
				<a class="more" href="{{ route('sektor') }}">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
	</div>
	@endif
	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet light ">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-sharp hide"></i>
						<span class="caption-subject font-green-sharp bold uppercase">Struktur Organisasi</span>
					</div>
					<div class="actions">
						
					</div>
				</div>
				<div class="portlet-body">
					<h3>{{ !empty($profiles->title) ? $profiles->title : '' }}</h3>
					<p>{{ !empty($profiles->deskripsi) ? $profiles->deskripsi : '' }}</p> <br>
					<table style="font-family:calibri;" class="table table-bordered table-hover" id="profile">
						<thead>
							<tr>
								<th>No</th>
								<th width="100">Foto</th>
								<th>Nama</th>
								<th>Jabatan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>
									@if($profiles->foto_kepala_balai1 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kepala_balai1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@else
									<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
									@endif
								</td>
								<td>
									{{ !empty($profiles->nama_kepala_balai1) ? $profiles->nama_kepala_balai1 : '' }}
								</td>
								<td>
									Kepala Balai PPW Maluku
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>
									@if($profiles->foto_kasubag != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasubag)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@else
									<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
									@endif
								</td>
								<td>
									{{ !empty($profiles->nama_kasubag) ? $profiles->nama_kasubag : '' }}
								</td>
								<td>
									Kasubag Tata Usaha
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>
									@if($profiles->foto_kasie_wil1 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil1)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@else
									<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
									@endif
								</td>
								<td>
									{{ !empty($profiles->nama_kasie_wil1) ? $profiles->nama_kasie_wil1 : '' }}
								</td>
								<td>
									Kasie Pelaksana Wilayah 1
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>
									@if($profiles->foto_kasie_wil2 != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasie_wil2)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@else
									<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
									@endif
								</td>
								<td>
									{{ !empty($profiles->nama_kasie_wil2) ? $profiles->nama_kasie_wil2 : '' }}
								</td>
								<td>
									Kasie Pelaksana Wilayah 2
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>
									@if($profiles->foto_kasatker != null)
									<img src="{{url('uploads/foto/'.$profiles->id.'/'.$profiles->foto_kasatker)}}" class="preview_image" width="200" height="200" style="object-fit: cover;" /><br><br>
									@else
									<img src="{{url('assets/global/img/not_found_foto.jpg')}}" width="200" height="200">
									@endif
								</td>
								<td>
									{{ !empty($profiles->nama_kasatker) ? $profiles->nama_kasatker : '' }}
								</td>
								<td>
									Kasataker Pelaksanaan PPW Maluku
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript">
	function redirectPerencanaanVerifikasi () {
		document.location.href="perencanaan?status_penilaian=1&filter=submit";
	}

	function redirectPerencanaanNotVerifikasi () {
		document.location.href="perencanaan?status_penilaian=0&filter=submit";
	}
</script>
@endsection