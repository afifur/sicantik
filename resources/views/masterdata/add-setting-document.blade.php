@extends('app')
@section('title', 'Setting Document')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('sektor') }}">Setting Document</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-setting-document', !empty($sektor->id) ? $sektor->id : '') }}">Setting Document</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="icon-doc font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase">Setting Document ( {{ !empty($sektor->nama_sektor) ? $sektor->nama_sektor : '' }} )</span>
		</div>
		<div class="actions">
			<a href="{{ route('sektor') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-setting-kegiatan" action="{{ route('create-setting-document') }}" method="POST">
			@csrf
			<input type="hidden" name="id_sektor" id="id_sektor" value="{{ !empty($sektor->id) ? $sektor->id : '' }}">
			<a href="#" id="btn-add" style="margin-bottom:10px;" class="btn btn-circle btn-sm blue"><i class="fa fa-plus"></i> Tambah Document</a>
			<table class="table table-bordered append-file">
				<tr style="font-family:calibri;">
					<th>Nama Dokumen</th>
					<th>Tampilkan Upload File</th>
					<th>Aksi</th>
				</tr>
				@if(count($setting) > 0)
					@foreach($setting as $detailFile)
					<tr class="user_data">
						<td width="350">
							<input required type="text" name="nama_kegiatan[]" id="nama_kegiatan" class="form-control" value="{{ $detailFile->nama_kegiatan }}" placeholder="Nama Kegiatan">
						</td>
						<td width="100">
							<select required class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload">
								<option value=""></option>
								<option value="1" {{ $detailFile->view_upload == 1 ? "selected": "" }}>YA</option>
								<option value="0" {{ $detailFile->view_upload == 0 ? "selected": "" }}>TIDAK</option>
							</select>
						</td>
						<td width="50">
							<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>
						</td>
					</tr>
					@endforeach
				@else
				<tr class="user_data">
					<td width="350">
						<input type="text" name="nama_kegiatan[]" id="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan">
					</td>
					<td width="100">
						<select class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload">
							<option value="1">YA</option>
							<option value="0">TIDAK</option>
						</select>
					</td>
					<td width="50">
						<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>
					</td>
				</tr>
				@endif
			</table>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	var count = 2; 
	$("#btn-add").click(function(e){
		count++;
		e.preventDefault();
		$(".append-file").append(
			'<tr class="user_data">'
				+'<td width="350">'
					+'<input required type="text" name="nama_kegiatan[]" id="nama_kegiatan'+count+'" class="form-control" placeholder="Nama Kegiatan">'
				+'</td>'
				+'<td width="100">'
					+'<select required class="form-control select2" data-placeholder="tampilkan upload file" name="view_upload[]" id="view_upload'+count+'">'
						+'<option value="1">YA</option>'
						+'<option value="0">TIDAK</option>'
					+'</select>'
				+'</td>'
				+'<td width="50">'
					+'<a href="javascript:void(0);" class="btn red remove_item"><i class="fa fa-remove"></i></a>'
				+'</td>'
			+'</tr>'
	    );

		$('#view_upload'+count).select2({
	   		width:'100%',
	   	});
	});

	$('.append-file').on('click', '.remove_item', function(e) {
	    e.preventDefault();

	    $(this).parents('.user_data').remove();
	});

	function sumData () {
		var number = [];
		$("input[name='nama_kegiatan[]']").each(function() {
			number.push($(this).val());
		});

		return number.length;
	}

	function validateData () {
		var result = true;
		$("input[name='nama_kegiatan[]']").each(function() {
			var nama_kegiatan = $(this).val();
			if (nama_kegiatan == "") {
				result = false;
			}
		});

		return result;
	}

	$("#btn-submit-act").click(function(){
		if (sumData() == 0) {
			alert("belum ada data yang diisi");
		}else if(validateData() == false){
			alert("silahkan lengkapi data");
		}else{
			$("#form-submit-setting-kegiatan").submit();
		}
		
	});
</script>
@endsection