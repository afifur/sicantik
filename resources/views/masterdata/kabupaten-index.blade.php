@extends('app')
@section('title', '- Kabupaten')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('kabupaten') }}">Kabupaten</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Kabupaten</span>
			<span class="caption-helper">daftar kabupaten</span>
		</div>
		<div class="actions">
			<a href="{{ route('add-kabupaten') }}" class="btn btn-circle btn-primary btn-sm">
				<i class="fa fa-plus"></i> Tambah Kabupaten 
			</a> &nbsp;
			<a href="{{ route('add-provinsi') }}" class="btn btn-circle btn-warning btn-sm">
				<i class="fa fa-plus"></i> Tambah Provinsi 
			</a> &nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif
		<div class="table-toolbar">
			<table class="table table-striped table-bordered table-hover datatables" id="kabupaten">
				<thead>
					<tr>
						<th>No</th>
						<th>Provinsi</th>
						<th>Nama Kabupaten</th>
						<th>Status</th>
						<th width="80" style="text-align:right;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
						<tr style="font-family:calibri;">
							<td>{{ $loop->iteration }}</td>
							<td>{{ $values->nama_provinsi }}</td>
							<td>{{ $values->nama_kabupaten }}</td>
							<td>
								@if($values->status == 1)
									<span class="label label-sm label-success">Aktif</span>
								@else
									<span class="label label-sm label-danger">Non Aktif</span>
								@endif
							</td>
							<td align="right">
								<a href="{{ route('edit-kabupaten', $values->id) }}" class="btn btn-sm blue tooltips" data-placement="left" data-original-title="Edit kabupaten"><i class="fa fa-edit"></i></a>
								<a href="{{ route('delete-kabupaten', $values->id) }}" onClick="return confirm('ingin menghapus data ini ?');" class="btn btn-sm red tooltips" data-placement="left" data-original-title="Hapus kabupaten"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection