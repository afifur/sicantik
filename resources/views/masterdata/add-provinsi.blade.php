@extends('app')
@section('title', '-'.empty($provinsi->id) ? 'Tambah':'Edit'.' Provinsi')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('kabupaten') }}">Kabupaten</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-provinsi') }}">@php echo empty($provinsi->id) ? 'Tambah':'Edit' @endphp Provinsi</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($provinsi->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($provinsi->id) ? 'Tambah':'Edit' @endphp Provinsi</span>
		</div>
		<div class="actions">
			<a href="{{ route('kabupaten') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif

		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-provinsi" action="{{ route('create-provinsi') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Nama provinsi</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="hidden" name="id" id="id" value="{{ !empty($provinsi->id) ? $provinsi->id : '' }}">
								<input required type="text" name="nama_provinsi" id="nama_provinsi" value="{{ !empty($provinsi->nama_provinsi) ? $provinsi->nama_provinsi : old('nama_provinsi') }}" class="form-control" placeholder="Nama provinsi">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Status</label>
							<select required class="form-control select2me" name="status" id="status">
								<option value="">pilih status</option>
								@php
									$selectedStatusActive = "";
									if(isset($provinsi->status) && $provinsi->status == "1"){
										$selectedStatusActive = "selected";
									}else if(old('status') == "1"){
										$selectedStatusActive = "selected";
									}

									$selectedStatusNonActive = "";
									if(isset($provinsi->status) && $provinsi->status == "0"){
										$selectedStatusNonActive = "selected";
									}else if(old('status') == "0"){
										$selectedStatusNonActive = "selected";
									}
								@endphp
								<option value="1" {{ $selectedStatusActive }}>Aktif</option>
								<option value="0" {{ $selectedStatusNonActive }}>Non Aktif</option>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>&nbsp;</label> <br>
							<button type="submit" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</form>

		<div class="table-toolbar">
			<h4>List Provinsi</h4>
			<table class="table table-striped table-bordered table-hover datatables" id="provinsi">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Provinsi</th>
						<th>Status</th>
						<th width="80" style="text-align:right;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
						<tr style="font-family:calibri;">
							<td>{{ $loop->iteration }}</td>
							<td>{{ $values->nama_provinsi }}</td>
							<td>
								@if($values->status == 1)
									<span class="label label-sm label-success">Aktif</span>
								@else
									<span class="label label-sm label-danger">Non Aktif</span>
								@endif
							</td>
							<td align="right">
								<a href="#modal-edit" data-toggle="modal" data-id="{{ $values->id }}" data-name="{{ $values->nama_provinsi }}" data-status="{{ $values->status }}" onclick="getDetailProvinsi(this)" class="btn btn-sm blue tooltips" data-placement="left" data-original-title="Edit provinsi"><i class="fa fa-edit"></i></a>
								<a href="{{ route('delete-provinsi', $values->id) }}" onClick="return confirm('ingin menghapus data ini ?');" class="btn btn-sm red tooltips" data-placement="left" data-original-title="Hapus provinsi"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="modal-edit" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Provinsi</h4>
			</div>
			<div class="modal-body">
				<form role="form" action="{{ route('create-provinsi') }}" method="POST">
					@csrf
					<div class="form-group">
						<label>Nama Provinsi</label>
						<input type="hidden" name="id" id="edit_id">
						<input type="text" name="nama_provinsi" id="edit_nama_provinsi" class="form-control">
					</div>
					<div class="form-group">
						<label>Status</label>
						<select class="form-control select2me" name="status" id="edit_status">
							<option value="">pilih status</option>
							<option value="1">Aktif</option>
							<option value="0">Non Aktif</option>
						</select>
					</div>
			</div>
			<div class="modal-footer">
					<button type="submit" class="btn btn-circle green"><i class="fa fa-save"></i> Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	function getDetailProvinsi (element) {
		var id = $(element).data('id');
		var name = $(element).data('name');
		var status = $(element).data('status');

		$("#edit_id").val(id);
		$("#edit_nama_provinsi").val(name);
		$("#edit_status").val(status).trigger('change');
	}
</script>
@endsection