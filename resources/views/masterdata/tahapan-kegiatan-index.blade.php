@extends('app')
@section('title', '- Tahapan Kegiatan')

@section('content')

<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('tahapan-kegiatan') }}">Tahapan Kegiatan</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="fa fa-docs font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> Tahapan Kegiatan</span>
			<span class="caption-helper">daftar Tahapan Kegiatan</span>
		</div>
		<div class="actions">
			<a href="{{ route('add-tahapan-kegiatan') }}" class="btn btn-circle btn-primary btn-sm">
				<i class="fa fa-plus"></i> Tambah Baru 
			</a> &nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($success = Session::get('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $success }}</strong>
			</div>
		@endif
		<div class="table-toolbar">
			<table class="table table-striped table-bordered table-hover datatables" id="tahapan-kegiatan">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Tahapan Kegiatan</th>
						<th>Keterangan</th>
						<th>Status</th>
						<th width="80" style="text-align:right;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data) > 0)
						@foreach($data as $values)
					<tr style="font-family:calibri;">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $values->nama_tahapan_kegiatan }}</td>
						<td>{{ $values->keterangan }}</td>
						<td>
							@if($values->status == 1)
								<span class="label label-sm label-success">Aktif</span>
							@else
								<span class="label label-sm label-danger">Non Aktif</span>
							@endif
						</td>
						<td align="right">
							<a href="{{ route('edit-tahapan-kegiatan', $values->id) }}" class="btn btn-sm blue tooltips" data-placement="left" data-original-title="Edit Tahapan Kegiatan"><i class="fa fa-edit"></i></a>
							<a href="{{ route('delete-tahapan-kegiatan', $values->id) }}" onClick="return confirm('ingin menghapus data ini ?');" class="btn btn-sm red tooltips" data-placement="left" data-original-title="Hapus Tahapan Kegiatan"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection