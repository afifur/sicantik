@extends('app')
@section('title', '-'.empty($kabupaten->id) ? 'Tambah':'Edit'.' Kabupaten')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('kabupaten') }}">Kabupaten</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-kabupaten') }}">@php echo empty($kabupaten->id) ? 'Tambah':'Edit' @endphp Kabupaten</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($kabupaten->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($kabupaten->id) ? 'Tambah':'Edit' @endphp Kabupaten</span>
		</div>
		<div class="actions">
			<a href="{{ route('kabupaten') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-kabupaten" action="{{ route('create-kabupaten') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Provinsi</label>
							<select class="form-control select2me" data-placeholder="pilih provinsi" name="provinsi_id" id="provinsi_id">
								<option value=""></option>
								@if(count($provinsi) > 0)
									@foreach($provinsi as $prov)
									@php
										$selectedProv = "";
										if(!empty($kabupaten->provinsi_id) && $kabupaten->provinsi_id == $prov->id){
											$selectedProv = "selected";
										}else if(old('provinsi_id') == $prov->id){
											$selectedProv = "selected";
										}
									@endphp
									<option value="{{ $prov->id }}" {{ $selectedProv }}>{{ $prov->nama_provinsi }}</option>
									@endforeach
								@endif
							</select>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control select2me" name="status" id="status">
								<option value="">pilih status</option>
								@php
									$selectedStatusActive = "";
									if(isset($kabupaten->status) && $kabupaten->status == "1"){
										$selectedStatusActive = "selected";
									}else if(old('status') == "1"){
										$selectedStatusActive = "selected";
									}

									$selectedStatusNonActive = "";
									if(isset($kabupaten->status) && $kabupaten->status == "0"){
										$selectedStatusNonActive = "selected";
									}else if(old('status') == "0"){
										$selectedStatusNonActive = "selected";
									}
								@endphp
								<option value="1" {{ $selectedStatusActive }}>Aktif</option>
								<option value="0" {{ $selectedStatusNonActive }}>Non Aktif</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama kabupaten</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="hidden" name="id" id="id" value="{{ !empty($kabupaten->id) ? $kabupaten->id : '' }}">
								<input type="text" name="nama_kabupaten" id="nama_kabupaten" value="{{ !empty($kabupaten->nama_kabupaten) ? $kabupaten->nama_kabupaten : old('nama_kabupaten') }}" class="form-control" placeholder="Nama kabupaten">
							</div>
						</div>
					</div>		
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#provinsi_id").val() == "") {
			alert("provinsi belum dipilih");
		}else if ($("#nama_kabupaten").val() == "") {
			alert("nama kabupaten harus diisi");
		}else if($("#status").val() == ""){
			alert("status belum dipilih");
		}else{
			$("#form-submit-kabupaten").submit();
		}
		
	});
</script>
@endsection