@extends('app')
@section('title', '-'.empty($users->id) ? 'Tambah':'Edit'.' Jenis Kegiatan')

@section('content')
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			<a href="{{ route('dashboard') }}">Dashboard</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('jenis-kegiatan') }}">Jenis Kegiatan</a>
			<i class="fa fa-angle-right"></i>
		</li>
		<li>
			<a href="{{ route('add-jenis-kegiatan') }}">@php echo empty($users->id) ? 'Tambah':'Edit' @endphp Jenis Kegiatan</a>
		</li>
	</ul>
</div> <br>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption font-blue-sharp">
			<i class="@php echo empty($users->id) ? 'icon-plus':'icon-note' @endphp font-blue-sharp"></i>  
			<span class="caption-subject bold uppercase"> @php echo empty($users->id) ? 'Tambah':'Edit' @endphp Jenis Kegiatan</span>
		</div>
		<div class="actions">
			<a href="{{ route('jenis-kegiatan') }}" class="btn btn-circle grey"><i class="fa fa-arrow-left"></i> Kembali</a>
			<button type="button" id="btn-submit-act" class="btn btn-circle blue"><i class="fa fa-save"></i> Simpan</button>&nbsp;
			<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body">
		@if ($error = Session::get('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
				<strong>{{ $error }}</strong>
			</div>
		@endif

		@if (count($errors) > 0)
        <div class="alert alert-danger">
        	<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
            <ol>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ol>
        </div>
        @endif

        <form role="form" id="form-submit-kegiatan" action="{{ route('create-jenis-kegiatan') }}" method="POST">
			@csrf
			<div class="form-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Nama Kegiatan</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="hidden" name="id" id="id" value="{{ !empty($kegiatan->id) ? $kegiatan->id : '' }}">
								<input type="text" name="nama_kegiatan" id="nama_kegiatan" value="{{ !empty($kegiatan->nama_kegiatan) ? $kegiatan->nama_kegiatan : old('nama_kegiatan') }}" class="form-control" placeholder="Nama Kegiatan">
							</div>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select class="form-control select2me" name="status" id="status">
								<option value="">pilih status</option>
								@php
									$selectedStatusActive = "";
									if(isset($kegiatan->status) && $kegiatan->status == "1"){
										$selectedStatusActive = "selected";
									}else if(old('status') == "1"){
										$selectedStatusActive = "selected";
									}

									$selectedStatusNonActive = "";
									if(isset($kegiatan->status) && $kegiatan->status == "0"){
										$selectedStatusNonActive = "selected";
									}else if(old('status') == "0"){
										$selectedStatusNonActive = "selected";
									}
								@endphp
								<option value="1" {{ $selectedStatusActive }}>Aktif</option>
								<option value="0" {{ $selectedStatusNonActive }}>Non Aktif</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Keterangan</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"></i></span>
								<input type="text" name="keterangan" id="keterangan" value="{{ !empty($kegiatan->keterangan) ? $kegiatan->keterangan : old('keterangan') }}" class="form-control" placeholder="Keterangan">
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
	$("#btn-submit-act").click(function(){
		if ($("#nama_kegiatan").val() == "") {
			alert("nama kegiatan harus diisi");
		}else if($("#status").val() == ""){
			alert("status belum dipilih");
		}else{
			$("#form-submit-kegiatan").submit();
		}
		
	});
</script>
@endsection