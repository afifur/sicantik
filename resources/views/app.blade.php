<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
  		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<title>Sicantik @yield('title')</title>

		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>

		<link href="{{url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css"/>

		<link href="{{url('assets/admin/pages/css/tasks.css')}}" rel="stylesheet" type="text/css"/>

		<link rel="stylesheet" type="text/css" href="{{url('assets/global/plugins/bootstrap-select/bootstrap-select.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{url('assets/global/plugins/select2/select2.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{url('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}"/>
		
		<link href="{{url('assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
		<link href="{{url('assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="{{url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>

		

		@section('css')
  		@show
	</head>
		<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
			@include('layout.header')
			<div class="page-container">
				@include('layout.sidebar')
				<div class="page-content-wrapper">
					<div class="page-content">
		    			@yield('content')
		    		</div>
		    	</div>
		    	@include('layout.content')
		  	</div>
		  	@include('layout.footer')

		  	<script src="{{url('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
			
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery.pulsate.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/bootstrap-daterangepicker/moment.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/pages/scripts/index.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/pages/scripts/tasks.js')}}" type="text/javascript"></script>

			<script type="text/javascript" src="{{url('assets/global/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
			<script type="text/javascript" src="{{url('assets/global/plugins/select2/select2.min.js')}}"></script>
			<script type="text/javascript" src="{{url('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
			
			<script type="text/javascript" src="{{url('assets/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
			<script type="text/javascript" src="{{url('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>

			@section('js')
 			@show

 			@if(session()->get('type') == 1 && session()->exists('client') == null)
 			<script type="text/javascript">
 				$('#modal-add-session').modal('show');
 			</script>
 			@endif

			<script>
				jQuery(document).ready(function() {    
				   Metronic.init(); // init metronic core componets
				   Layout.init(); // init layout
				   QuickSidebar.init(); // init quick sidebar
				   Demo.init(); // init demo features
				   Index.init();   
				   Index.initDashboardDaterange();
				   // Index.initJQVMAP(); // init index page's custom scripts
				   Index.initCalendar(); // init index page's custom scripts
				   Index.initCharts(); // init index page's custom scripts
				   Index.initChat();
				   Index.initMiniCharts();
				   Tasks.initDashboardWidget();

				   $('.select2').select2({
				   		width:'100%'
				   });

				   $('.select2me').select2({
				   		width:'100%',
				   });
				   $('.datatables').DataTable();
				});

				$('.format-number').keyup(function(event) {
			        // skip for arrow keys
			        if(event.which >= 37 && event.which <= 40) return;

			        // format number
			        $(this).val(function(index, value) {
				        return value
				        .replace(/\D/g, "")
				        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
				        ;
			        });
			    });
			</script>
		</body>
</html>