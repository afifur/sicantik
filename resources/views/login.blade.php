<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Sicantik - Login</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta content="" name="ceklis id"/>
		<meta content="" name="author"/>

		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/admin/pages/css/login.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/css/components.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{url('assets/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
		<link href="{{url('assets/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
		
		<link rel="shortcut icon" href="{{url('assets/admin/layout/img/favicon.png')}}"/>
	</head>
		<body class="login">
			<div class="menu-toggler sidebar-toggler"></div>
			<div class="logo">
				<a href="{{ route('dashboard') }}">
					<img src="{{url('assets/global/img/logo_sicantik.png')}}" width="400" height="80"><br><br><br>
				</a>
			</div>
			<div class="content" style="margin-top:-30px;">
				@if ($message = Session::get('error'))
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
				<form class="login-form" action="{{ route('process-login') }}" method="post">
					@csrf
					<h3 class="form-title">Log In</h3>
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						<span>
						Enter any username and password. </span>
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Username</label>
						<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" id="username" />
					</div>
					<div class="form-group">
						<label class="control-label visible-ie8 visible-ie9">Password</label>
						<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" />
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success uppercase">Login</button>
					</div>
				</form>
			</div>

			<script src="{{url('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
			<script src="{{url('assets/admin/pages/scripts/login.js')}}" type="text/javascript"></script>
			<script>
				jQuery(document).ready(function() {     
				Metronic.init(); // init metronic core components
				Layout.init(); // init current layout
				Login.init();
				Demo.init();
				});
			</script>
		</body>
</html>