@extends('app')
@section('title', '- Dashboard')

@section('content')
	@if ($success = Session::get('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert"><h3>×</h3></button> 
			<strong>{{ $success }}</strong>
		</div>
	@endif
	@if(count($contents) > 0)
		@if(count($contents) == 1)
			<img src="{{url('uploads/slide/'.$contents[0]->file_content)}}" class="preview_image" width="100%" style="object-fit: cover;" />
		@else
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				
				<div class="carousel-inner">
					@foreach($contents as $keyx => $slide)
						@php
							$activeKeyx = "";
							if($keyx == 0){
								$activeKeyx = "active";
							}
						@endphp
						<div class="item {{ $activeKeyx }}">
							<img src="{{url('uploads/slide/'.$slide->file_content)}}" class="preview_image" width="100%" style="object-fit: cover;" />
						</div>
					@endforeach
				</div>

				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
			</div>
		@endif
	@endif
	<br><br>
	@if(session()->get('type') == 2)
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat blue-madison">
				<div class="visual">
					<i class="fa fa-comments"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($totalUsers) }}
					</div>
					<div class="desc" style="font-size:13px;">
						 Total Users
					</div>
				</div>
				<a class="more" href="{{ route('users') }}">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat red-intense">
				<div class="visual">
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($perencanaanVerifikasi) }}
					</div>
					<div class="desc" style="font-size:13px;">
						 Total Perencanaan (Sudah Verifikasi)
					</div>
				</div>
				<a class="more" href="#" onclick="redirectPerencanaanVerifikasi()">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat green-haze">
				<div class="visual">
					<i class="fa fa-shopping-cart"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ number_format($perencanaanNotVerifikasi) }}
					</div>
					<div class="desc" style="font-size:13px;">
						Total Perencanaan (Belum Verifikasi)
					</div>
				</div>
				<a class="more" href="#" onclick="redirectPerencanaanNotVerifikasi()">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat purple-plum">
				<div class="visual">
					<i class="fa fa-globe"></i>
				</div>
				<div class="details">
					<div class="number" >
						 {{ number_format($totalSektor) }}
					</div>
					<div class="desc" style="font-size:13px;">
						Total Sektor
					</div>
				</div>
				<a class="more" href="{{ route('sektor') }}">
				View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
	</div>
	@endif
	<div class="clearfix"></div>
@endsection

@section('js')
<script type="text/javascript">
	function redirectPerencanaanVerifikasi () {
		document.location.href="perencanaan?status_penilaian=1&filter=submit";
	}

	function redirectPerencanaanNotVerifikasi () {
		document.location.href="perencanaan?status_penilaian=0&filter=submit";
	}
</script>
@endsection