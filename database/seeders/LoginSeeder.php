<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('login')->insert(
        	[
	            'name' => 'Admin Kab/Kota',
	            'username' => 'adminkabkota',
	            'password' => sha1("123"),
	            'type' => 1,
	            'status' => 1,
	            'created_at' => date('Y-m-d H:i:s'),
	            'created_by' => 1
        	],
        	[
	            'name' => 'Admin Balai',
	            'username' => 'adminbalai',
	            'password' => sha1("123"),
	            'type' => 2,
	            'status' => 1,
	            'created_at' => date('Y-m-d H:i:s'),
	            'created_by' => 1
        	]
        );
    }
}
