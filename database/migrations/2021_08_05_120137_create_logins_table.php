<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('phone', 30)->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('type')->comment="1 => Admin Kab/Kota, 2 => Admin Balai";
            $table->integer('status');
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login');
    }
}
