<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 150);
            $table->text('deskripsi')->nullable();
            $table->string('nama_kepala_balai1', 150)->nullable();
            $table->string('jabatan_kepala_balai1', 150)->nullable();
            $table->string('foto_kepala_balai1', 255)->nullable();
            
            $table->string('nama_kepala_balai2', 150)->nullable();
            $table->string('jabatan_kepala_balai2', 150)->nullable();
            $table->string('foto_kepala_balai2', 255)->nullable();

            $table->string('nama_kasubag', 150)->nullable();
            $table->string('jabatan_kasubag', 150)->nullable();
            $table->string('foto_kasubag', 255)->nullable();

            $table->string('nama_kasie_wil1', 150)->nullable();
            $table->string('jabatan_kasie_wil1', 150)->nullable();
            $table->string('foto_kasie_wil1', 255)->nullable();

            $table->string('nama_kasie_wil2', 150)->nullable();
            $table->string('jabatan_kasie_wil2', 150)->nullable();
            $table->string('foto_kasie_wil2', 255)->nullable();

            $table->string('nama_kasatker', 150)->nullable();
            $table->string('jabatan_kasatker', 150)->nullable();
            $table->string('foto_kasatker', 255)->nullable();

            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
