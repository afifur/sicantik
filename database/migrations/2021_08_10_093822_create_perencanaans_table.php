<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerencanaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perencanaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_perencanaan', 50)->nullable();
            $table->date('tgl_perencanaan');
            $table->integer('id_paket');
            $table->string('nama_paket', 255);
            $table->integer('id_sektor');
            $table->integer('id_kabupaten');
            $table->integer('status')->default(1);
            $table->string('periode', 50);
            $table->integer('id_jenis_kegiatan');
            $table->integer('id_tahapan_kegiatan');
            $table->integer('id_kategori');
            $table->string('lokasi', 150)->nullable();
            $table->string('volume', 100)->nullable();
            $table->string('satuan', 100)->nullable();
            $table->string('nilai_apbn', 255)->default(0);
            $table->double('score_all')->default(0);
            $table->text('keterangan')->nullable();
            $table->integer('status_penilaian')->default(0)->comment = "1 => Sudah dinilai, 0 => Belum Dinilai";
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perencanaan');
    }
}
