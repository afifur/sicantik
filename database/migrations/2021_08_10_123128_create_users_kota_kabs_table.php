<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersKotaKabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_kota_kab', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_kabupaten');
            $table->integer('id_login')->nullable();
            $table->string('email', 50)->nullable();
            $table->string('no_telp', 50)->nullable();
            $table->text('alamat')->nullable();
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_kota_kab');
    }
}
