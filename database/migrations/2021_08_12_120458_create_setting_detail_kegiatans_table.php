<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingDetailKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_detail_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_sektor');
            $table->string('nama_kegiatan', 255)->nullable();
            $table->integer('view_upload')->default(0)->comment = "1 => Ya, 0 => Tidak";
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_detail_kegiatan');
    }
}
