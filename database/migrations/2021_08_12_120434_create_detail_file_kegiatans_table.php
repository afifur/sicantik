<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailFileKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_file_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('perencanaan_id');
            $table->integer('id_paket')->nullable();
            $table->integer('id_detail_paket')->nullable();
            $table->string('nama_kegiatan', 255)->nullable();
            $table->string('file_dokumen', 255)->nullable();
            $table->integer('status_dokumen')->default(0)->comment = "1 => Ya, 0 => Tidak";
            $table->double('score_dokumen')->default(0);
            $table->integer('penilaian_per_doc')->nullable()->comment = "1 => OK, 2 => Revisi";
            $table->text('catatan')->nullable();
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_file_kegiatan');
    }
}
