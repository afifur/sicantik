<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTahapanKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahapan_kegiatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_tahapan_kegiatan', 50)->nullable();
            $table->string('nama_tahapan_kegiatan', 255);
            $table->text('keterangan')->nullable();
            $table->integer('status')->comment = '1 => active, 0 => non active';
            $table->timestamps($precision = 0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahapan_kegiatan');
    }
}
